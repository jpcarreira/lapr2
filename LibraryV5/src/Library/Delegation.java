/* 
 * LAPR2 PROJECT
 * INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO
 * JUNE 2012
 * 
 * @authors: Diogo Leite, João Carreira, Paulo Ponciano
 * 
 */



package Library;

import java.io.Serializable;

/**
 * CLASS DELEGATION: creates objects for each delegation that participates in
 * the Olympic Games
 */

public class Delegation implements Serializable {
    
    // ***** CLASS VARIABLES *****
    
    private static int numberOfGames=0;
    
    // ***** INSTANCE VARIABLES *****
    
    // delegation's name 
    private String delegation;
    // delegation acronym
    private String acro;
    // medals
    private int gold;
    private int silver;
    private int bronze;
    
    
    // ***** CONSTRUCTORS *****
    
    /**
     * Complete constructor
     * @param delegation
     * @param acro (acronym)
     */
   
    public Delegation(String delegation, String acro){
        setDelegation(delegation);
        setAcro(acro);
    }

    
    // ***** ACCESS METHODS (GETS) *****
    public String getDelegation() {
        return delegation;
    }
    
    public String getAcro(){
        return acro;
    }
    
    public int getGold() {
        return gold;
    }
    
    public int getSilver() {
        return silver;
    }
    
    public int getBronze() {
        return bronze;
    }
    
    // ***** CLASS METHODS *****
    
    /*
     * Method to determine the total number of medals of a given delegation
     */
    
        // ***** CLASS METHODS *****
    
    /**
     * Method to clear all medals
     * @param d Container<Delegation>
     */
    
    public static void clearMedals(Container<Delegation> d){
        for(int i=0; i<d.getsizeActual(); i++){
            d.get(i).setGold(0);
            d.get(i).setSilver(0);
            d.get(i).setBronze(0);
        }
    }
    
    /**
     * Return the total medals of the Delegation
     * @param d
     * @return int
     */
    public static int totalMedals(Delegation d){
        return d.getGold()+d.getSilver()+d.getBronze();
    }
    
    
    /**
     * Method to determina de total number of medals of all delegations in container
     * @param c Container<Delegation>
     */
    public static void totalMedalsAllDelegations(Container<Delegation> c){
        for(int i=0; i<c.getsizeActual(); i++)
            totalMedals(c.get(i));
    }
    
    /**
     * Method to sort delegations by alphabetical order
     * @param d Container<Delegation>
     */
    public static void sortDelegationsAlpha(Container<Delegation> d){
        boolean sorted = false;
        while(!sorted){
            sorted=true;
            for(int i=0; i<d.getsizeActual()-1; i++){
                if(d.get(i).getDelegation().compareToIgnoreCase(d.get(i+1).getDelegation())>0){
                    Delegation aux = d.get(i+1);
                    d.set(d.get(i), i+1);
                    d.set(aux,i);
                    sorted=false;
                }
            }
        }
    }
    
    /**
     * Method to sort delegations by total medals won (in descendent order)
     * @param d Container<Delegation>
     */
    public static void sortDelegationsByMedals(Container<Delegation> d){
        boolean sorted = false;
        while(!sorted){
            sorted=true;
            for(int i=0; i<d.getsizeActual()-1; i++){
                if(totalMedals(d.get(i))<totalMedals(d.get(i+1))){
                    Delegation aux = d.get(i+1);
                    d.set(d.get(i), i+1);
                    d.set(aux,i);
                    sorted=false;
                }
            }
        }
    }
    
    
    /**
     * Method to sort delegations by gold medals won (in descendent order)
     * @param d Container<Delegation>
     */
    public static void sortDelegationsByGoldMedals(Container<Delegation> d){
        boolean sorted = false;
        while(!sorted){
            sorted=true;
            for(int i=0; i<d.getsizeActual()-1; i++){
                if(d.get(i).getGold()<d.get(i+1).getGold()){
                    Delegation aux = d.get(i+1);
                    d.set(d.get(i), i+1);
                    d.set(aux,i);
                    sorted=false;
                }
            }
        }
    }
    
    
    /**
     * Method to sort delegations by silver medals won (in descendent order)
     * @param d Container<Delegation>
     */
    public static void sortDelegationsBySilverMedals(Container<Delegation> d){
        boolean sorted = false;
        while(!sorted){
            sorted=true;
            for(int i=0; i<d.getsizeActual()-1; i++){
                if(d.get(i).getGold()==d.get(i+1).getGold()){
                    if(d.get(i).getSilver()<d.get(i+1).getSilver()){
                        Delegation aux = d.get(i+1);
                        d.set(d.get(i), i+1);
                        d.set(aux,i);
                        sorted=false;
                }
                }
            if(d.get(i).getGold()!=d.get(i).getGold())
                break;
            }
        }
    }
    
    
    /**
     * Method to sort delegations by Bronze medals won (in descendent order)
     * @param d Container<Delegation>
     */
    public static void sortDelegationsByBronzeMedals(Container<Delegation> d){
        boolean sorted = false;
        while(!sorted){
            sorted=true;
            for(int i=0; i<d.getsizeActual()-1; i++){
                if(d.get(i).getSilver()==d.get(i+1).getSilver()){
                    if(d.get(i).getBronze()<d.get(i+1).getBronze()){
                        Delegation aux = d.get(i+1);
                        d.set(d.get(i), i+1);
                        d.set(aux,i);
                        sorted=false;
                    }   
                }
            }
        }
    }
    
     
    /**
     * Method to untie delegations with the same amount of medals won
     * @param d Container<Delegation>
     */
    public static void untieBronzeMedalists(Container<Delegation> d){
        boolean sorted = false;
        while(!sorted){
            sorted=true;
            for(int i=0; i<d.getsizeActual()-1; i++){
                if(d.get(i).getBronze()==d.get(i+1).getBronze()){
                    if(totalMedals(d.get(i))<totalMedals(d.get(i+1))){
                        Delegation aux = d.get(i+1);
                        d.set(d.get(i), i+1);
                        d.set(aux,i);
                        sorted=false;
                    }   
                }
            }
        }
    }
    
    
    /**
     * Method to rank delegations using gold>silver>bronze>totalMedals criteria
     * @param d Container<Delegation>
     */
    public static void rankDelegations(Container<Delegation> d){
        boolean sorted = false;
        while(!sorted){
            sorted=true;
            sortDelegationsByGoldMedals(d);
            sortDelegationsBySilverMedals(d);
            sortDelegationsByBronzeMedals(d);
            untieBronzeMedalists(d);
        }
    }
    
    /*
     * Method to assign medals for a given for a given year
     */

    public static void assignMedalsForSportForYear(Container<Result> r, Container<Event> e, Container<Delegation> d, int y){
        Container<Result> copy = filterByYear(r,y);
        Result.assignMedalsForSport(copy,e,d);
    }
    
    
    /**
     * Method to filter results for a given year
     * @param r
     * @param y
     * @return 
     */
    public static Container<Result> filterByYear(Container<Result> r, int y){
        Container<Result> copy = new Container<Result>();
        for(int i=0; i<r.getsizeActual();i++){
            if(r.get(i).getEvent().getYear()==y)
                copy.add(r.get(i));
        }
        return copy;
    }
    
    /**
     * Method to determine how many games the delegation participated
     * @param r
     * @param s
     * @return 
     */
    public static int numberOfGames(Container<Result> r, String s){
        int[] years = new int[500];
        for(int i=0; i<r.getsizeActual(); i++){
            int c=0;
            if(r.get(i).getParticipant().getDelegation().getDelegation().equalsIgnoreCase(s)){
                years[c]=r.get(i).getEvent().getYear();
                c++;
            }
        }
        for(int i=0; i<years.length-1; i++){
            for(int j=i+1; j<years.length; j++){
                if(years[j]>years[i]){
                    int tmp=years[i];
                    years[i]=years[j];
                    years[j]=tmp;
                 }
             }
        }
        for(int i=0; i<years.length;i++){
                System.out.println(years[i]);
                numberOfGames++;
        }
        return numberOfGames;
    }
    
    // ***** MODIFICATION METHODS (SETS) *****
    public void setDelegation (String delegation) {
        this.delegation = delegation.toUpperCase();
    }
    
    public void setAcro(String acro){
        this.acro = acro.toUpperCase();
    }
    
    public void setGold(int gold) {
        this.gold = gold;
    }
    
    public void setSilver(int silver) {
        this.silver = silver;
    }
    
    public void setBronze(int bronze) {
        this.bronze = bronze;
    }
    
    
    // ***** ACESSORY METHODS *****
    
    // toString 
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nDelegation: ");
        s.append(delegation);
        s.append("\nAcronym: ");
        s.append(acro);
        s.append("\nGold medals: ");
        s.append(gold);
        s.append("\nSilver medals: ");
        s.append(silver);
        s.append("\nBronze medals: ");
        s.append(bronze);
        return s.toString();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Delegation other = (Delegation) obj;
        if ((this.delegation == null) ? (other.delegation != null) : !this.delegation.equals(other.delegation)) {
            return false;
        }
        return true;
    }
}
