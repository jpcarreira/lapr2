/* 
 * LAPR2 PROJECT
 * INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO
 * JUNE 2012
 * 
 * @authors: Diogo Leite, João Carreira, Paulo Ponciano
 * 
 */
/* TO DO LIST:
 * 
 * 
 * 
 */
package Library;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Serializable;
import java.util.Scanner;
import javax.swing.JOptionPane;

/*
 * All methods to import data from de several csv files are in this class
 */
public class CsvImport implements Serializable {

    /*
     * Import Delegations
     *
     */
    public static void save_deleg(String caminho, Container delegation) throws FileNotFoundException {
        boolean conteudo = false;
        Scanner ler = new Scanner(new FileReader(caminho));
        String texto = "", ciglas_antigas = "";

        //Skip the first line of the csv
        ler.nextLine();
        //csv reading cycle

        while (ler.hasNextLine()) {
            texto = ler.nextLine();
            String[] vec = texto.split(";");
            //if the line contains more than one acronyms

            if (vec.length > 2) {
                //tira espaços
                ciglas_antigas = vec[2].replaceAll(" ", "");
                //eliminates all parentheses
                ciglas_antigas = ciglas_antigas.replaceAll("\\(.*?\\)", "");
                //create a new array with all the acronyms old
                String[] separar = ciglas_antigas.split(",");
                //joins the old acronyms currently used
                vec[0] = vec[0] + "," + ciglas_antigas;

                String texto_a = vec[0], txt = "";
                //eliminates spaces to simplify searches
                texto_a = texto_a.replaceAll(" ", "");


                //passes everything to the principal vector of which the pos 0 we have the acronyms and names in a post in full
                vec[0] = texto_a;
                vec[0] = vec[0].trim();
                vec[1] = vec[1].trim();

                //**************Verificação se esta repedito ou não
                conteudo = Verificacoes.Verificacoes_Delegation(delegation, vec[1]);
                //System.out.println(conteudo + " se ja foi repetido");
                if (conteudo == false) {
                    delegation.add(new Delegation(vec[1], vec[0]));
                }
                //System.out.println(delegation);


                //Delegation a = new Delegation(vec[0],vec[1]);

            } else {
                //System.out.println("Delegação " + vec[0]+ "Acronym " + vec[1]);

                for (int i = 0; i < vec.length; i++) {
                    texto = vec[i];
                    texto = texto.trim();
                    vec[i] = texto;
                }
                delegation.add(new Delegation(vec[1], vec[0]));
            }
            //delegation.add(new Delegation("ola_a","ola_b"));
        }
        //System.out.println(delegation);
        ler.close();
        //delegation.listar();
        //Save_individual.save();

    }

    //PARA TESTAR
    public static int pesquisa_abv(Container<Delegation> z, String abv) {
        String extenco_pais = "";
        int pos = -1;
        String pais;
        //System.out.println(abv + "  ola");
        for (int i = 0; i < z.getsizeActual(); i++) {
            String[] acroni = z.get(i).getAcro().split(",");
            pais = z.get(i).getAcro();
            for (int j = 0; j < acroni.length; j++) {
                if ((acroni[j].compareTo(abv)) == 0) {
                    extenco_pais = z.get(i).getDelegation();
                    pos = i;
                    //System.out.println(pos+" posição "+ abv + " abreviação");
                    break;
                }
            }
        }

        return pos;
    }

    public static int pesquisa_delegation(Container<Delegation> z, String pais) {
        String pais_content = "", aux;
        int pos = -1;
        for (int i = 0; i < z.getsizeActual(); i++) {
            pais_content = z.get(i).getDelegation().toString();
            if (pais_content.compareToIgnoreCase(pais) == 0) {
                pos = i;
            }

        }
        return pos;
    }

    //**************************************************************************
    /*
     * Import Disciplines
     */
    public static void save_discipline_content(String caminho_fich, Container<Discipline> disciplina_content, Container<Sport> sport) throws FileNotFoundException {
        String texto = "", caminho = caminho_fich;
        Scanner leitura = new Scanner(new FileReader(caminho_fich));
        int posicao_sport_list = 0, tipo_resultado = -1;
        boolean men = false, women = false, mixed = false, conteudo = false;
        leitura.nextLine();
        while (leitura.hasNextLine()) {
            men = false;
            women = false;
            mixed = false;
            texto = leitura.nextLine();
            //System.out.println(texto);
            String vec[] = texto.split(";");
            posicao_sport_list = pesquisa_abv_sport(sport, vec[0].trim());
            if (posicao_sport_list != -1) {
                Sport desporto = sport.get(posicao_sport_list);
                //System.out.println(vec[3]);
                if (vec[3].compareToIgnoreCase("X") == 0) {
                    men = true;
                }
                if (vec[4].compareToIgnoreCase("X") == 0) {
                    women = true;
                }
                if (vec[5].compareToIgnoreCase("X") == 0) {
                    mixed = true;
                }
                vec[1] = vec[1].trim();
                vec[2] = vec[2].trim();
                //System.out.println(vec[1]);
                tipo_resultado = tipo_calssificacoes(vec[6]);
                Discipline disciplina = new Discipline(vec[1], vec[2], desporto, men, women, mixed, tipo_resultado, vec[7]);

                //**************Verificação se tudo o que nessesario existe
                conteudo = Verificacoes.Verificacoes_discipline(disciplina_content, vec[1]);
                //System.out.println(conteudo);


                if (conteudo == false) {
                    disciplina_content.add(disciplina);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Esta disciplina não tem Desporto: " + vec[1]);
            }


            //System.out.println(disciplina);
        }
    }

    public static int pesquisa_abv_discipline(Container<Discipline> z, String abv) {
        String extenco_pais = "";
        int pos = -1;
        String disciplin;
        //System.out.println("O parametro que recebe é: "+abv);
        for (int i = 0; i < z.getsizeActual(); i++) {

            disciplin = z.get(i).getDiscipline().toString();

            if (disciplin.compareTo(abv) == 0) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    public static int tipo_calssificacoes(String tipo_result) {
        int tipo = -1;
        String tipo_restula = tipo_result.trim();
        if (tipo_restula.compareToIgnoreCase("time") == 0) {
            tipo = 0;
            //System.out.println("tipo 0");
        } else if (tipo_restula.compareToIgnoreCase("points") == 0) {
            tipo = 2;
            //System.out.println("tipo 2");
        } else if (tipo_restula.compareToIgnoreCase("m, ft") == 0) {
            tipo = 1;
            //System.out.println("tipo 1");
        } else if (tipo_restula.compareToIgnoreCase("rank") == 0) {
            tipo = 3;
            //System.out.println("tipo 3");
        }
        return tipo;
    }

    public static int pesquisa_tipo_resultado(Container<Discipline> z, String disciplina) {
        int a = -1;
        String nom_disciplina = "";
        //System.out.println(disciplina + " recebo isto");
        for (int i = 0; i < z.getsizeActual(); i++) {
            if (nom_disciplina.compareToIgnoreCase("Team") == 0) {

                break;
            } else {
                nom_disciplina = z.get(i).getDiscipline().toString();
                //System.out.println(nom_disciplina + "nome da disciplina");
                if (nom_disciplina.compareToIgnoreCase(disciplina) == 0) {
                    a = z.get(i).getResult_type();
                    //System.out.println(a + " posiçaõ");
                    break;
                }

            }

            /*
             * if(nom_disciplina.compareTo(disciplina)==0){ a =
             * z.getElemento(i).getResult_type(); }
             */
        }
        return a;
    }

    //**************************************************************************
    /*
     * Import events
     */
    public static void save_event(String nom_fich, Container<Event> eventos, Container<Discipline> disciplina) throws FileNotFoundException {
        //Container delegation = new Container();
        String texto = "", ficheiro = nom_fich;
        //System.out.println(" ano   ffadfaadfasdfasdfasfasdf");
        boolean men = false, women = false, mixt_tipe = false, conteudo = false;
        int pos = 0, ano_jogo = 0;
        Scanner ler = new Scanner(new FileReader(ficheiro));
        String[] ano = ficheiro.split("_");

        ano[3] = ano[3].substring(0, 4);
        //System.out.println(ano[3] + " ano   ffadfaadfasdfasdfasfasdf");
        ler.nextLine();
        while (ler.hasNextLine()) {
            men = false;
            women = false;
            mixt_tipe = false;
            texto = ler.nextLine();
            //System.out.println(texto + " Conteudo linha");
            String[] vec = texto.split(";");

            if (vec.length >= 5) {
                if (vec[4].compareToIgnoreCase("X") == 0) {
                    mixt_tipe = true;
                }
            }
            if (vec[2].compareToIgnoreCase("X") == 0) {
                men = true;
            }
            if (vec.length >= 4) {
                if (vec[3].compareToIgnoreCase("X") == 0) {
                    women = true;
                }
            }
            vec[1] = vec[1].trim();
            //System.out.println("Valor do vector: " + vec[1]);

            pos = pesquisa_abv_discipline(disciplina, vec[1]);
            //System.out.println("posição: "+ pos);


            Discipline disciplina_a = disciplina.get(pos);
            //System.out.println("asdasdasd");
            //verifica se o conteudo nessesario está todo la dentre (no array) e se não existe nada a duplicar (normalemente não há)
            conteudo = Verificacoes.Verificacoes_event(disciplina, disciplina_a.getDiscipline().toString(), men, women, mixt_tipe);
            if (conteudo == true) {
                ano_jogo = Integer.parseInt(ano[3]);
                Event evento = new Event(vec[1], disciplina_a, ano_jogo, men, women, mixt_tipe);
                eventos.add(evento);
            }


        }

    }

    public static int procura_event_com_ano(Container<Event> z, int ano, String nome) {
        int a = -1;
        String nome_evento = "";
        int test;
        Event aaa;

        for (int i = 0; i < z.getsizeActual(); i++) {
            //z.get(i).getYear()==ano && z.get(i).getDiscipline().getDiscipline().toString().compareToIgnoreCase(nome)==0
            if (z.get(i).getYear() == ano && z.get(i).getDiscipline().getDiscipline().toString().compareToIgnoreCase(nome) == 0) {
                a = i;
                break;
            }

            z.get(i).getYear();
        }
        return a;
    }

    //**************************************************************************
    /*
     * Import individuals
     */
    public static void save_individ(String caminho, Container<Participant> jogador, Container<Delegation> pais) throws FileNotFoundException {
        boolean conteudo = false;
        Scanner leitura = new Scanner(new FileReader(caminho));
        String texto = "", name = "";
        int posicao_ext_joga = 0;
        boolean team = false;
        leitura.nextLine();
        while (leitura.hasNextLine()) {

            texto = leitura.nextLine();
            String[] vec = texto.split(";");
            vec[0] = vec[0].trim();
            if (vec[0].compareToIgnoreCase("Team") != 0) {

                
                if (team == true) {
                    name = vec[1].trim();
                    //System.out.println(vec_a[0] + " valor vec 0");
                    name = name.replaceAll("\\(.*?\\)", "");
                    name = name.trim();
                    name = name.trim();
                    //vec[1] = vec[1].replaceAll("\\(.*?\\)", "");
                    posicao_ext_joga = pesquisa_abv(pais, name);
                    posicao_ext_joga = posicao_ext_joga;
                    //System.out.println("ola B");
                    //System.out.println("posição team: "+ posicao_ext_joga);
                } else {
                    String[] vec_a = vec[1].split(",");
                    //System.out.println(vec_a[0] + " valor vec 0");
                    name = vec_a[0].trim();
                    vec_a[1] = vec_a[1].trim();
                    posicao_ext_joga = pesquisa_abv(pais, vec_a[1]);
                    //Delegation test = pais.get(posicao_ext_joga);
                    name = vec_a[0].trim();
                    //System.out.println("ola A");
                }
                //System.out.println("Name: "+vec_a[0]);
                //System.out.println(name + ": este é o nome");

                //********
                //System.out.println(posicao_ext_joga + " esta");
                if (posicao_ext_joga != -1) {
                    Delegation test = pais.get(posicao_ext_joga);
                    Participant j = new Participant(name, test);
                    // Verifica se existe a delegação dele e se ele ainda nao foi repetido
                    conteudo = Verificacoes.Verificacoes_inivid(jogador, name);
                    //System.out.println("Conteudo é: " + conteudo);
                    //
                    if (conteudo == false) {
                        jogador.add(j);
                    }
                } else {
                    //* TRADUZIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
                    JOptionPane.showMessageDialog(null, "Este Jogador não tem delegação " + name);
                }

            } else {
                team = true;
            }
            /*
             *
             * String[] vec = texto.split(";"); //System.out.println(vec[1]);
             * if(vec[0].compareTo("Team")!= 0){ System.out.println("oi gato");
             * String[] vec_a = vec[1].split(","); vec_a[1] = vec_a[0].trim();
             * System.out.println(vec_a[0] + " linha"); }
             */
        }



    }

    public static int pesquisa_competitro(Container<Participant> z, String abv) {
        int pos = -1;
        String jogador = "";
        for (int i = 0; i < z.getsizeActual(); i++) {
            //System.out.println("oi gato");
            jogador = z.get(i).getName();
            //System.out.println(jogador);

            if (jogador.compareToIgnoreCase(abv) == 0) {
                pos = i;
                //System.out.println(i);
                break;
            }

        }
        return pos;
    }

    //**************************************************************************
    /*
     * Import Results
     */
    public static void save_result(String caminho, Container<Result> resultados, Container<Event> eventos, Container<Participant> competidor, Container<Discipline> disciplina) throws FileNotFoundException {
        //Contentor delegation = new Contentor();
        String nome_fich = caminho;

        Scanner ler = new Scanner(new FileReader(nome_fich));
        String texto = "", aux = "", desporto = "", resultado_obt = "";
        Participant test = null;

        String[] ano_vec = caminho.split("_");
        boolean equipa = false;
        //String[] vec_ano = new String[4];
        //contagem para disciplinas
        int tamanho_max = 0;
        tamanho_max = ano_vec[0].length();
        ano_vec[0] = ano_vec[0].substring(tamanho_max - 4, tamanho_max);
        //System.out.println(ano_vec[0]);
        int ano = Integer.parseInt(ano_vec[0]), posicao_evento, posicao_jogador, tipo_resultado_disci;
        String[] ano_descobre = nome_fich.split("_");
        String nome_do_jogador = "";
        //Skip the first line of the csv
        ler.nextLine();
        //csv reading cycle
        while (ler.hasNextLine()) {
            texto = ler.nextLine();
            String[] vec = texto.split(";");
            //System.out.println(texto);


            aux = vec[0].trim();
            if (aux.compareTo("") != 0) {
                desporto = aux.trim();
            }
            posicao_evento = procura_event_com_ano(eventos, ano, desporto);
            //System.out.println(posicao_evento + ": posição " + desporto + " : desporto");

            //verificação se é team
            //System.out.println(vec[0]);
            vec[0] = vec[0].trim();
            if (vec[0].compareToIgnoreCase("Team") == 0) {
                equipa = true;
                //System.out.println("equipa:: "+ equipa);
            }
            if (posicao_evento != -1 && vec[0].compareToIgnoreCase("Team") != 0) {
                //System.out.println(equipa +" equipa???");

                if (equipa == false) {
                    String[] nom_jogador = vec[1].split(",");
                    nome_do_jogador = nom_jogador[0].trim();
                } else {
                    nome_do_jogador = vec[1];
                    nome_do_jogador = nome_do_jogador.replaceAll("\\(.*?\\)", "");
                    nome_do_jogador = nome_do_jogador.trim();
                    //System.out.println("nome do jogador: "+nome_do_jogador);
                }
                //******************
                //resultado_obt = vec[2].trim();
                //System.out.println("Resultado: "+ resultado_obt);
                //System.out.println("Nome do jogador: "+nome_do_jogador);

            }
            posicao_jogador = pesquisa_competitro(competidor, nome_do_jogador);
            //System.out.println("Posição: "+ posicao_jogador);
            //para não haver erro
            if (posicao_jogador != -1) {
                test = competidor.get(posicao_jogador);
                //System.out.println(test);
            }

            tipo_resultado_disci = pesquisa_tipo_resultado(disciplina, desporto);
            //evitar erros caso -1
            if (posicao_evento != -1) {
                Event evento_final = eventos.get(posicao_evento);
                if (vec[2].compareToIgnoreCase("") != 0) {
                    Result resultado_final = new Result(evento_final, test, vec[2], tipo_resultado_disci);
                    //System.out.println(resultado_final);
                    resultados.add(resultado_final);
                }

            }
            /*
             *
             *
             * //System.out.println(texto + vec.length);
             *
             *
             *
             * if(posicao_evento!=-1){
             *
             * String[] nom_jogador = vec[1].split(",");
             * //System.out.println(nom_jogador[0]); nom_jogador[0] =
             * nom_jogador[0].trim(); posicao_jogador =
             * Save_individual.pesquisa_competitro(competidor, nom_jogador[0]);
             * if(posicao_jogador != -1){ test =
             * competidor.get(posicao_jogador); //System.out.println(test); }
             *
             * //System.out.println(eventos.get(posicao_evento)); }
             * resultado_obt = vec[2].trim();
             *
             * tipo_resultado_disci =
             * Save_discipline.pesquisa_tipo_resultado(disciplina, desporto);
             * if(tipo_resultado_disci!= -1){
             * System.out.println(tipo_resultado_disci + " :este é o tipo"); }
             *
             * if(posicao_evento != -1){ Event evento_final =
             * eventos.get(posicao_evento); //Competitor competidor_final =
             * competidor.get(ano); Result resultado_final = new
             * Result(evento_final, test, vec[2], tipo_resultado_disci);
             * System.out.println(resultado_final);
             * //resultados.add(resultado_final); }
             */
            
        }
//System.out.println(resultados);
    }

    public static int tipo_classifica(String resultado) {
        int tipo = 2;
        if (resultado.contains("m") || resultado.contains("ft")) {
            tipo = 1;
        } else if (resultado.contains(".")) {
            if (resultado.length() <= 5) {
                tipo = 0;
            } else {
                tipo = 2;
            }
        }
        if (resultado.contains(":")) {
            tipo = 0;
        }


        return tipo;
    }

    //**************************************************************************
    /*
     * Import Sports
     */
    public static void save_sport_cat(String caminho_ficheiro, Container desporto_cat) throws FileNotFoundException {
        String texto = "", desporto = "", caminho = caminho_ficheiro;
        Scanner ler = new Scanner(new FileReader(caminho_ficheiro));

        boolean conteudo = false;
        Sport test_conteudo = new Sport("");
        //Skip the first line of the csv
        ler.nextLine();
        //csv reading cycle

        while (ler.hasNextLine()) {
            texto = ler.nextLine();
            String[] vec = texto.split(";");
            desporto = vec[0];
            test_conteudo.setSport(desporto);
            //System.out.println(desporto);
            // test
            // só presisa verifica que existe o desporto assosciado a disciplina
            if (!desporto_cat.toString().contains(test_conteudo.toString())) {
                desporto_cat.add(new Sport(desporto));
            }
            conteudo = desporto_cat.contains(test_conteudo);
        }

    }

    public static int pesquisa_abv_sport(Container<Sport> z, String abv) {
        String extenco_pais = "";
        int pos = -1;
        String sport;
        for (int i = 0; i < z.getsizeActual(); i++) {
            sport = z.get(i).getSport().toString();
            if (sport.compareTo(abv) == 0) {
                pos = i;
                break;
            }
        }
        return pos;
    }
}
