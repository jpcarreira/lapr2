/* 
 * LAPR2 PROJECT
 * INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO
 * JUNE 2012
 * 
 * @authors: Diogo Leite, João Carreira, Paulo Ponciano
 * 
 */

package Library;

import java.io.Serializable;

/** 
 * CLASS DISCIPLINE: creates objects representing a given discipline of the 
 * Olympic Games; for discipline is understood an event that occurs in all Games
 * editions; it's associated to SPORT
 */

public class Discipline implements Serializable {

    //  *** CLASS VARIABLES ***

    
    //  *** INSTANCE VARIABLES ***
    
    // discipline's name (e.g., 100m freestyle)
    private String discipline;
    // sport to which discipline is related
    
    private String type;
    
    private Sport sport;
    
    private boolean men;
    
    private boolean women;
    
    private boolean mixed;
    
    private int result_type;
    
    private String order;
  
    
    //  *** CONSTRUCTORS ***

   
    /**
     * complete constructor
     * @param discipline
     * @param type
     * @param sport
     * @param men
     * @param women
     * @param mixed
     * @param result_type
     * @param order 
     */
    public Discipline(String discipline, String type, Sport sport, boolean men, boolean women, boolean mixed, int result_type, String order){
        setDiscipline(discipline);
        setType(type);
        setSport(sport);
        setMen(men);
        setWomen(women);
        setMixed(mixed);
        setResult_type(result_type);
        setOrder(order);
    }

    //  *** CLASS METHODS ***

   
/**
 * Method to assign medals for a given for a given discipline
 * @param r
 * @param e
 * @param d
 * @param s 
 */
    public static void assignMedalsForSportForDiscipline(Container<Result> r, Container<Event> e, Container<Delegation> d, String s){
        Container<Result> copy = filterByDiscipline(r,s);
        Result.assignMedalsForSport(copy,e,d);
    }
    
    
    /**
     * Method to filter results for a given discipline
     * @param r
     * @param s
     * @return 
     */
    public static Container<Result> filterByDiscipline(Container<Result> r, String s){
        Container<Result> copy = new Container<Result>();
        for(int i=0; i<r.getsizeActual();i++){
            if(r.get(i).getEvent().getDiscipline().getDiscipline().equalsIgnoreCase(s))
                copy.add(r.get(i));
        }
        return copy;
    }

    //  *** INSTANCE METHODS ***

    //      *** ACCESS METHODS (GETS) ***

    public String getDiscipline() {
        return discipline;
    }
    
    public String getType() {
        return type;
    }
    
    public Sport getSport() {
        return sport;
    }
    
    public boolean getMen() {
        return men;
    }
    
    public boolean getWomen() {
        return women;
    }
    
    public boolean getMixed() {
        return mixed;
    }
    
    public int getResult_type() {
        return result_type;
    }
    
   public String getOrder() {
        return order;
    }
    
    
    //      *** MODIFICATION METHODS (SETS) ***

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public void setSport(Sport sport) {
        this.sport = sport;
    }
    
    public void setMen(boolean men) {
        this.men = men;
    }
    
    public void setWomen(boolean women) {
        this.women = women;
    }
    
    public void setMixed(boolean mixed) {
        this.mixed = mixed;
    }
    
    public void setResult_type(int result_type) {
        this.result_type = result_type;
    }
    
    public void setOrder(String order) {
        this.order = order;
    }
        
     
    //      *** ACCESSORY METHODS ***

    // toString
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nDiscpline: ");
        s.append(discipline);
        s.append("\nType: ");
        s.append(type);
        s.append(sport);
        s.append("\nMen: ");
        s.append(men);
        s.append("\nWomen: ");
        s.append(women);
        s.append("\nMixed: ");
        s.append(mixed);
        s.append("\nResult Type: ");
        s.append(result_type);
        s.append("\nType order: ");
        s.append(order);
        return s.toString();
    }

}
