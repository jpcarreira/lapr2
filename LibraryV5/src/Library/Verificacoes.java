/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Library;

/**
 *
 * @author Diogo Leite
 */
public class Verificacoes {

    public static boolean Verificacoes_Delegation(Container<Delegation> pais, String nome) {
        //boolean a = false;
        int tamanho = pais.getsizeActual();
        //System.out.println(tamanho + " String"); 
        if (tamanho > 2) {
            for (int i = 0; i < pais.getsizeActual(); i++) {
                //System.out.println(pais.get(i).getDelegation().toString() + " String\n\n"); 
                //System.out.println(nome + " este é o nome");
                if (pais.get(i).getDelegation().toString().compareTo(nome) == 0) {
                    return true;
                }
            }
        }


        return false;
    }

    public static boolean Verificacoes_inivid(Container<Participant> jogadores, String nome) {
        boolean a = false;
        int tamanho = jogadores.getsizeActual();
        if (tamanho > 2) {
            for (int i = 1; i < jogadores.getsizeActual(); i++) {
                if (jogadores.getElemento(i).getName().toString().compareToIgnoreCase(nome) == 0) {
                    a = true;
                    return a;
                }
            }
        }
        return a;
    }

    public static boolean Verificacoes_discipline(Container<Discipline> disciplina_content, String nome) {
        //boolean a = false;
        int tamanho = disciplina_content.getsizeActual();
        //System.out.println(tamanho + " String"); 
        if (tamanho > 2) {

            for (int i = 0; i < disciplina_content.getsizeActual(); i++) {
                //System.out.println(disciplina_content.get(i).getDiscipline().toString() + " String\n\n"); 
                //System.out.println(nome + " este é o nome");
                if (disciplina_content.get(i).getDiscipline().toString().compareTo(nome) == 0) {
                    return true;
                } 
                /*
                else {
                    return false;
                }
*/
            }

        }


        return false;
    }

    public static boolean Verificacoes_event(Container<Discipline> disciplina, String nome, boolean men, boolean women, boolean mix) {
        //boolean a = false;
        int tamanho = disciplina.getsizeActual();
        //System.out.println(tamanho + " String"); 
        if (tamanho > 2) {

            for (int i = 0; i < disciplina.getsizeActual(); i++) {
                //System.out.println(disciplina.get(i).getDiscipline().toString() + " Valor vector"); 
                //System.out.println(nome + " este é o nome\n");
                if (disciplina.get(i).getDiscipline().toString().compareTo(nome) == 0) {
                    //System.out.println("MEN vec: " + disciplina.get(i).getMen() + " MEN vec: " +men); 
                    if(disciplina.get(i).getMen() == true && men == true || disciplina.get(i).getMen() == true && men == false || disciplina.get(i).getMen() == false && men == false){
                        //System.out.println("WOMEN vec: " + disciplina.get(i).getWomen() + " WOMEN vec: " +women); 
                      if(disciplina.get(i).getWomen() == true && men == true || disciplina.get(i).getWomen() == true && men == false || disciplina.get(i).getWomen() == false && women == false){
                          //System.out.println("MIX vec: " + disciplina.get(i).getMixed() + " MEN MIX: " +mix); 
                          if(disciplina.get(i).getMixed() == true && men == true || disciplina.get(i).getMixed() == true && men == false || disciplina.get(i).getMixed() == false && mix == false){
                              return true;
                          }
                            
                      }
                        
                    }
                    
                    //System.out.println("encontrou");
                } 
            }


        }
        //System.out.println("Não encontrou");
        return false;


    }
    
     public static boolean Verificacoes_result(Container<Discipline> disciplina, String nome, boolean men, boolean women, boolean mix) {
         return false;
     }
}
