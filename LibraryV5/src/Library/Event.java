/* 
 * LAPR2 PROJECT
 * INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO
 * JUNE 2012
 * 
 * @authors: Diogo Leite, João Carreira, Paulo Ponciano
 * 
 */

/* TO DO LIST:
 *  1) add comments to instance variables
 *  2) add comments to delAnalysisDisciplines
 *  3) override equals()
 */

package Library;

import java.io.Serializable;

/**
 * CLASS EVENT: creates objects for a given event of the Olympic Games (for event
 * it's considered a sporting event that occurs only in a particular edition
 * of the Olympic Games); it's composed of DISCIPLINE
 */

public class Event  implements Serializable {

    //  *** CLASS VARIABLES ***
    
    //  *** INSTANCE VARIABLES ***
    // discipline's name (e.g., "200m freestyle", will often have the same name as the discipline
    private String event;
    // discipline's name (e.g., "200m freestyle"
    private Discipline discipline;
    // date (year) corresponding to the event (e.g., 2004)
    private int year;
    
    private boolean men;
    private boolean women;
    private boolean mixed;
    

    //  *** CONSTRUCTOR ***
    
    /**
     * complete construtor
     * @param event
     * @param discipline
     * @param year
     * @param men
     * @param women
     * @param mixed 
     */
    public Event(String event, Discipline discipline, int year, boolean men, boolean women, boolean mixed) {
        setEvent(event);
        setDiscipline(discipline);
        setYear(year);
        setMen(men);
        setWomen(women);
        setMixed(mixed);
    }

    //  *** CLASS METHODS ***
    
    
    /**
     * Method to return which editions did the Delegation participated
     * (editions in years, e.g., 2004, 1988, etc...)
     * @param result
     * @param s
     * @return 
     */
    public static int[] delAnalysisGames(Container<Result> result, String s) {
        int[] aux = new int[50];
        int cont = 0;
        for (int j = 0; j < result.size(); j++) {
            if (result.get(j).getParticipant().getDelegation().getDelegation().equalsIgnoreCase(s)) {
                aux[cont] = result.get(j).getEvent().getYear();
                cont++;
            }
        }
        return aux;
    }
    
    
    
    /**
     * Method that verifies verifies if the discipline is present in a given result container
     * @param result
     * @param s
     * @return 
     */
    public static int[] delAnalysisDisciplines(Container<Result> result, String s)
    {
        int[] aux = new int[50];
        int cont = 0;
        String[] disciplineAux = new String[100];
        
        for (int j = 0; j < result.size(); j++) {
            if (result.get(j).getParticipant().getDelegation().getDelegation().equalsIgnoreCase(s)) {
                for (int i = 0; i < disciplineAux.length; i++) {
                    if(!disciplineAux[i].equalsIgnoreCase(result.get(j).getEvent().getDiscipline().getDiscipline()))
                    {
                        disciplineAux[cont] = result.get(j).getEvent().getDiscipline().getDiscipline();
                        cont++;
                    }
                }
            }
        }
        return aux;
    }
    
    //  *** INSTANCE METHODS ***
    
    //      *** ACCESS METHODS (GETS) ***
    public Discipline getDiscipline() {
        return discipline;
    }

    public String getEvent() {
        return event;
    }

    public int getYear() {
        return year;
    }

    public boolean getMen() {
        return men;
    }

    public boolean getWomen() {
        return women;
    }

    public boolean getMixed() {
        return mixed;
    }

    //      *** MODIFICATION METHODS (SETS) ***
    public void setDiscipline(Discipline discipline) {
        this.discipline = discipline;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMen(boolean men) {
        this.men = men;
    }

    public void setWomen(boolean women) {
        this.women = women;
    }

    public void setMixed(boolean mixed) {
        this.mixed = mixed;
    }

    //      *** ACCESSORY METHODS ***
    // toString
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("\nEvent: ");
        s.append(event);
        s.append(discipline);
        s.append("\nYear: ");
        s.append(year);
        return s.toString();
    }
}
