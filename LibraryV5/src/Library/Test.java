package Library;

import java.util.ArrayList;

public class Test {
    
    public static void main(String[] args) {
     
        /* 
         * Creating different objects
         */
        
        
        // delegations
        Delegation del1 = new Delegation("portugal","por");
        Delegation del2 = new Delegation("United kingdom","uk");
        Delegation del3 = new Delegation("bahrain","bah");
        Delegation del4 = new Delegation("united states", "usa");
        Delegation del5 = new Delegation("australia","aus");
        Delegation del6 = new Delegation("Jamaica","jam");
        
        // competitor
        Participant p1 = new Participant("Nélson Évora",del1);
        Participant p2 = new Participant("Philips Idowu",del2);
        Participant p3 = new Participant("Leevan Sands",del3);
        Participant p4 = new Participant("Michael Phelps",del4);
        Participant p5 = new Participant("Ian Thorpe",del5);
        Participant p6 = new Participant("Usain Bolt",del6);
        
        // sports
        Sport sp1 = new Sport("Athletics");
        Sport sp2 = new Sport("Swimming");

        
        // disciplines
        Discipline disc1 = new Discipline("Long jump","",sp1,true,false,false,0,"");
        Discipline disc2 = new Discipline("100m freestyle","",sp2,true,false,false,0,"");
        
        // events
        Event ev1 = new Event("Long jump",disc1,2004,true,false,false);
        Event ev2 = new Event("100m freestyle",disc2,2008,true,false,false);
        
        // results
        Result r1 = new Result(ev1,p1,"51.00 ft",1);
        Result r2 = new Result(ev1,p2,"50.90 ft",1);
        Result r3 = new Result(ev1,p3,"50.00 ft",1);
        Result r4 = new Result(ev2,p4,"57.96",0);
        Result r5 = new Result(ev2,p5,"57.79",0);
        Result r6 = new Result(ev2,p6,"57.70",0);
       
        //**********************************************************************
        
        
        /*
         * Testing class methods
         */
        
//        float f1 = Result.convertResultToFloat(r1);
//        float f2 = Result.convertResultToFloat(r2);
//        float f3 = Result.convertResultToFloat(r3);
//        float f4 = Result.convertResultToFloat(r4);
//        
//        System.out.println(f1);
//        System.out.println(f2);
//        System.out.println(f3);
//        System.out.println(f4);
        
        //**********************************************************************
        
        
        /*
         * Setting up containers
         */
        Container<Delegation> delegations = new Container<Delegation>();
        Container<Result> results = new Container<Result>();
        Container<Participant> athletes = new Container<Participant>();
        Container<Event> events = new Container<Event>();
//        
        delegations.add(del1);
        delegations.add(del2);
        delegations.add(del3);
        delegations.add(del4);
        delegations.add(del5);
        delegations.add(del6);
//        delegations.add(del7);
//        delegations.add(del8);
//        
//        results.add(r1);
//        results.add(r2);
//        results.add(r3);
        results.add(r4);
        results.add(r5);
        results.add(r6);
//        results.add(r7);
//        results.add(r8);
//        
        
        events.add(ev1);
        events.add(ev2);
        
//        athletes.add(cp1);
//        athletes.add(cp2);
//        athletes.add(cp3);
//        athletes.add(cp4);
//        athletes.add(cp5);
//        athletes.add(cp6);
        //**********************************************************************
        
        
        /*
         * Setting up arraylists
         */
        
        ArrayList<Result> arrayResults = new ArrayList<Result>();
        arrayResults.add(r1);
        arrayResults.add(r2);
        arrayResults.add(r3);
        arrayResults.add(r4);
        arrayResults.add(r5);
        arrayResults.add(r6);
        
       
        
        ArrayList<Delegation> arrayDelegations = new ArrayList<Delegation>();
        arrayDelegations.add(del1);
        arrayDelegations.add(del2);
        arrayDelegations.add(del3);
        arrayDelegations.add(del4);
        arrayDelegations.add(del5);
        arrayDelegations.add(del6);
        
        //**********************************************************************
      
        /*
         * Printing containers
         */
//        System.out.println("\n\n****** DELEGATIONS ******");
//        for(int i=0; i<delegations.size(); i++)
//            System.out.println(delegations.get(i));
//        
//        System.out.println("\n\n****** RESULTS ******");
//        for(int i=0; i<results.size(); i++)
//            System.out.println(results.get(i));
//        
//        System.out.println("\n\n****** ATHLETES ******");
//        for(int i=0; i<athletes.size(); i++)
//            System.out.println(athletes.get(i));
        //**********************************************************************
      
        
        /*
         * Method to convert time strings to floats 
         */
        
        // test string
//        String horas = "02:16:32";
//        String minutos = "1:44.82";
//        String segundos ="9.62";
//        String s="1:20.82";
        
        //**********************************************************************
        
        /*
         * Testing assignMedals
         */
//        Result.assignMedalsForEvent(results, ev1, delegations);
//        Result.assignMedalsForEvent(results, ev2, delegations);
//        
//      
//        System.out.println(del1);
//        System.out.println(del2);
//        System.out.println(del3);
//        System.out.println(del4);
//        System.out.println(del5);
//        System.out.println(del6);
//        
//        Result.assignMedalsForSport(results, events, delegations);
//        
//    
//        for(int i=0; i<delegations.getsizeActual(); i++){
//            if(delegations.get(i).getGold()!=0 ||delegations.get(i).getSilver()!=0|| delegations.get(i).getBronze()!=0){
//                System.out.println(delegations.get(i));
//            }
        
        //**********************************************************************
        
        /*
         * Testing sort by medals methods
         */
       
        Container<Delegation> del = new Container();
        del.add(del1);
        del.add(del2);
        del.add(del3);
        del.add(del4);
        
        del1.setGold(3);
        del1.setSilver(2);
        del1.setBronze(0);
        
        del2.setGold(3);
        del2.setSilver(1);
        del2.setBronze(0);
        
        del3.setGold(3);
        del3.setSilver(3);
        del3.setBronze(0);
        
        del4.setGold(3);
        del4.setBronze(1);
        del4.setBronze(1);
         
//        for(int j=0; j<del.getsizeActual(); j++){
//            System.out.println(del.get(j));
//        }
        
//        Result.sortDelegationsByGoldMedals(del);
        Delegation.rankDelegations(del);
        
        del.delete(2);
        
        for(int j=0; j<del.getsizeActual(); j++){
            System.out.println(del.get(j));
          
        }
         
        System.out.println(del.getsizeActual());
       
        
        
            
        //**********************************************************************
        
    }
        
        
    }


