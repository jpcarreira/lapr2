package Library;

import java.io.Serializable;

/**
 * This class codes for the dynamic array
 * @author Diogo Leite
 */
public class Container<E> implements Serializable {

    
    private static final int standard = 10;
    private int size;
    private int prox = 0;
    private E container[];
    private E aux[];

    
    /**
     * Constructor for the object Container 
     * @param n int size of the container
     */
    
    public Container(int n) {
        size = n;
        container = (E[]) new Object[size];
    }

    /**
     * Empty constructor 
     */
    public Container() {
        this(standard);
    }

    /** 
     * This method allows to set the size of the Container
     * @param size 
     */
    public void setsize(int size) {
        this.size = size;
    }

    /** 
     * This method sets an elements in the container
     * @param obj Object obj 
     * @param pos int index of the object
     */
    public void set(E obj, int pos) {
        container[pos] = obj;
    }

    /** 
     * This method return the object at index i
     * @param i int 
     * @return Object 
     */
    public E get(int i) {
        return container[i];
    }

    /** 
     * This method returns to total size of the container, including null references
     *   @return size int
     */
    public int size() {
        return size;
    }

    /**
     * This method return the size of the container (excluding null references)
     * @return prox int
     */
    public int getsizeActual() {
        return prox;
    }

    /** 
     * Similiar method to get, but this time considers the first index as 1
     * @param pos int 
     * @return Object 
     */
    public E getElemento(int pos) {
        return container[pos - 1];
    }

    /** 
     * This method makes an automatic setSize to the Container
     */
    public void setsize() {
        if (size < 101) {
            size = size * 2;
            reSize();
        } else {
            if (size < 1001) {
                size = size + 100;
                reSize();
            } else {
                size = size + 250;
                reSize();
            }
        }
    }

    /** 
     * This method ensures that all container's elements are maintained even when the container
     * increases in size
     */
    public void reSize() {
        aux = (E[]) new Object[prox];
        System.arraycopy(container, 0, aux, 0, prox);
        container = (E[]) new Object[size];
        System.arraycopy(aux, 0, container, 0, prox);
    }

    /** 
     * This methods adds objects to the container
     */
    public void add(E obj) {
        container[prox] = obj;
        prox++;
        if (size == prox) {
            setsize();
        }
    }

    /** 
     * This method add objects to the container at a given index
     * @param E obj, pos int 
     */
    public void add(E obj, int pos) {
        for (int i = prox + 1; i > pos - 1; i--) {
            container[i] = container[i - 1];
        }
        container[pos - 1] = obj;
        prox++;
        if (size == prox) {
            setsize();
        }
    }

    /** 
     * This method removes objects at a given index
     *  @param pos int 
     */
    public void delete(int pos) {
        if (size() > 0) {
            for (int i = pos; i < prox - 1; i++) {
                container[i] = container[i + 1];
            }
            container[prox] = null;
            prox--;
        }

        if (prox < (size / 2)) {
            setsize((size()) / 2);
        }
    }

    /** 
     * This method verifies if the container is empty
     * @return boolean 
     */
    public boolean empty() {
        if (prox > 0) {
            return false;
        } else {
            return true;
        }
    }

    /** 
     * This method list all objects in the container
     */
    public void list() {
        for (int i = 0; i < prox; i++) {
            if (container[i] != null) {
                System.out.println(container[i]);
            }
        }
    }

    /** 
     * toString() method
     * @return container.toString();
     */
    public String toString() {
        //  String s="Conteudo container: ";
        String s = "";
        for (int i = 0; i < prox; i++) {
            if (container[i] != null) {
                s += container[i];
            }
            if (i != prox - 1) {
                s += " ";
            }
        }
        return s;
    }

    /** 
     * This methods resizes the container to the actual size
     */
    public void DeleteActualPos() {
        aux = (E[]) new Object[prox];
        System.arraycopy(container, 0, aux, 0, prox);
        size = prox + 1;
        container = (E[]) new Object[size];
        System.arraycopy(aux, 0, container, 0, prox);
    }

    /** 
     * This method deletes all objects in the container
     */
    public void deleteAll() {
        if (size() != 0) {
            do {
                delete(0);
            } while (this.get(0) != null);

            this.setsize(standard);
            prox = 0;
        }

    }
    
    /** 
     * This method verifies whether the container has a given object
     * @param obj Object obj 
     * @return boolean 
     */
    public boolean contains(Object obj) {
        for (int i = 0; i < getsizeActual(); i++) {
            if (this.get(i).equals(obj)) {
                return true;
            }
        }
        return false;
    }
}
