/* 
 * LAPR2 PROJECT
 * INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO
 * JUNE 2012
 * 
 * @authors: Diogo Leite, João Carreira, Paulo Ponciano
 * 
 */


/* TO DO LIST:
 * 
 * 1) delete arraylist-based assignMedals (commented code)
 * 2) add comments to assingMedalsForEvent
 * 3) override equals()
 */

package Library;

import java.io.Serializable;


/**
 * CLASS RESULT: creates objects for each registered result of a given event in
 * the Olympic games; it can be elapsed time (e.g., Swimming 100m freestyle), 
 * obtained distance (e.g., Athletics javallin) or scored points (e.g., Gymnastics);
 */

public class Result implements Serializable {

    
    //  *** INSTANCE VARIABLES ***
    
    // event to which this result is related
    private Event event;
    // participant to which the result concerns 
    private Participant participant;
    // result (as string, as imported from csv)
    private String result;
    // type of result (flag; 0 for time-based result, 1 for distance-based result and 2 for points-based result
    private int type;
    

    //  *** CONSTRUCTORS ***

   
    /**
     * complete constructor
     * @param event
     * @param participant
     * @param result
     * @param type 
     */
    public Result (Event event, Participant participant, String result, int type){
        setEvent(event);
        setParticipant(participant);
        setResult(result);
        setType(type);
    }
 

    //  *** CLASS METHODS ***

    /* 
     * 
     */
/**
 * Method to covert the string-containg time result (format: hh:mm:ss) into 
 * float; time will be converted into seconds (as the sorting methods will 
 * work in seconds)
 * @param r
 * @return 
 */
     public static float convertResultToFloat(Result r){
        String s = r.getResult();
        // converting time results to floats, in seconds (type==0)
        if(r.getType()==0){
            float totalSeconds, seconds=0, minutes=0, hours=0;
            // if length less than 5 characters long then it's necessarily seconds
            if(s.length()<=5)
                seconds=Float.parseFloat(s);
            // if lenght more than 5 characters long then it can be either hours (hh:mm:ss) or minutes (m:ss or mm:ss)
            else{
                // if contains (.) then it's mm:ss or m:ss
                if(s.contains(".")){
                    int locationOfColon=s.indexOf(":");
                    minutes=Float.parseFloat(s.substring(0,locationOfColon));
                    seconds=Float.parseFloat(s.substring(locationOfColon+1,locationOfColon+6));
                }
                // else must be hh:mm:ss
                else{
                    hours=Float.parseFloat(s.substring(0,2));
                    minutes=Float.parseFloat(s.substring(3,5));
                    seconds=Float.parseFloat(s.substring(6,8));
                }
            }
            // return time in total seconds (even if it's the marathon)
            return totalSeconds=seconds+minutes*60+hours*3600;
            }
        // convert distance results to floats (type==1)
        else if(r.getType()==1){
            // result in feet
            if(s.contains(" ft")){
                int locationOfFt = s.indexOf(" ft");
                // converts distance to meters
                return Float.parseFloat(s.substring(0, locationOfFt))*0.3048f;
            }
            // result in meters
            else{
                int locationOfM = s.indexOf(" m");
                return Float.parseFloat(s.substring(0, locationOfM));
            } 
        }
        // convert points to floats
        else{
            return Float.parseFloat(s);
        }
     }
     
    
     /**
      * Method to assign medals in a given event of a particular sport
      * @param r
      * @param e
      * @param d 
      */
    public static void assignMedalsForEvent(Container<Result> r, Event e, Container<Delegation> d){
         // auxiliary array (predefined size to 500) to receive coverted results
         float[] aux = new float[500];
         // auxiliary array (predefined size to 500) to receive delegations names
         String[] aux2 = new String[500];
         int cont=0;
         int type=-1;
         for (int i=0; i<r.getsizeActual(); i++){
            if(r.get(i).getEvent().getEvent().equalsIgnoreCase(e.getEvent())){
                   type=r.get(i).getType(); 
                   aux[cont]=convertResultToFloat(r.get(i));
                   aux2[cont]=r.get(i).getParticipant().getDelegation().getDelegation();
                   cont++;
            }
         }
         if(type==0)
             sortAscend(aux, aux2);
         else if(type==1 || type==2){
             sortDescend(aux, aux2);
         }
         if(type==1 || type==2){
             for(int i=0; i<d.getsizeActual(); i++){
                if(aux2[0].equals(d.get(i).getDelegation())){
                    d.get(i).setGold(d.get(i).getGold()+1);
                }
                else if(aux2[1].equals(d.get(i).getDelegation())){
                    d.get(i).setSilver(d.get(i).getSilver()+1);
                }    
                else if(aux2[2].equals(d.get(i).getDelegation())){
                    d.get(i).setBronze(d.get(i).getBronze()+1);
                }    
            }
         }
         else if(type==0){
            for(int i=0; i<d.getsizeActual(); i++){
                if(aux2[aux2.length-1].equals(d.get(i).getDelegation())){
                    d.get(i).setBronze(d.get(i).getBronze()+1);
                }    
                else if(aux2[aux2.length-2].equals(d.get(i).getDelegation())){
                    d.get(i).setSilver(d.get(i).getSilver()+1);
                }    
                else if(aux2[aux2.length-3].equals(d.get(i).getDelegation())){
                    d.get(i).setGold(d.get(i).getGold()+1);
                }    
            }  
         }
         }
         
    
    /**
     * Method to assign medals in a given sport, for all events
     * @param r
     * @param e
     * @param d 
     */
    public static void assignMedalsForSport(Container<Result> r, Container<Event> e, Container<Delegation> d){
        for(int i=0; i<e.getsizeActual(); i++){
            assignMedalsForEvent(r, e.get(i),d);
        }
    }
     
     
      /**
       * Methods to sort results in ascendent order
       * @param f
       * @param s 
       */   
     public static void sortAscend(float[] f, String[] s){
         for(int i=0; i<f.length-1; i++){
             for(int j=i+1; j<f.length; j++){
                 if(f[j]<f[i]){
                     float tmp=f[i];
                     f[i]=f[j];
                     f[j]=tmp;
                     String tmp2=s[i];
                     s[i]=s[j];
                     s[j]=tmp2;
                 }
             }
        }
     }
     /**
      *  Methods to sort results in descendent order
      * @param f
      * @param s 
      */
    public static void sortDescend(float[] f, String[] s){
         for(int i=0; i<f.length-1; i++){
             for(int j=i+1; j<f.length; j++){
                 if(f[j]>f[i]){
                     float tmp=f[i];
                     f[i]=f[j];
                     f[j]=tmp;
                     String tmp2=s[i];
                     s[i]=s[j];
                     s[j]=tmp2;
                 }
            }
        }
     }
    

    //  *** INSTANCE METHODS ***

    //      *** ACCESS METHODS (GETS) ***
    
    public Event getEvent(){
        return event;
    }
    
    public Participant getParticipant(){
        return participant;
    }
    
    public String getResult(){
        return result;
    }
    
    public int getType(){
        return type;
    }
    
    
    //      *** MODIFICATION METHODS (SETS) ***

    public void setEvent(Event event){
        this.event = event;
    }
    
    public void setParticipant(Participant participant){
        this.participant = participant;
    }
    
    public void setResult(String result){
        this.result = result;
    }
    
    public void setType(int type){
        this.type = type;
    }
        
    
    //      *** ACCESSORY METHODS ***

    // toString
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nResult: ");
        if(type==0)
            s.append(result+" (time in seconds)");
        if(type==1)
            s.append(result+" (distance in centimeters)");
        if(type==2)
            s.append(result+" (points)");
        s.append(event);
        s.append(participant);
        return s.toString();
    }
    

}
