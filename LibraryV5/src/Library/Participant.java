/* 
 * LAPR2 PROJECT
 * INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO
 * JUNE 2012
 * 
 * @authors: Diogo Leite, João Carreira, Paulo Ponciano
 * 
 */




package Library;

import java.io.Serializable;

/**
 * CLASS PARTICIPANT: creats objects for each entity participating in the Olympic
 * Games, whether it's an individual or a team; an overload of constructores will
 * distinguish between INDIVIDUAL and TEAM
 */

public class Participant implements Serializable {
    
    // ***** INSTANCE VARIABLES *****
    
    // participant's name 
    private String name;
    // competitor's delegation
    private Delegation delegation;
    // medals
    private int gold;
    private int silver;
    private int bronze;
    
    
    // ***** CONSTRUCTORS *****
    
    /**
     * complete constructor
     * @param name
     * @param delegation 
     */
    public Participant(String name, Delegation delegation){
        setName(name);
        setDelegation(delegation);
    }
    
    
    // ***** CLASS METHODS *****
    
    /**
     * Method to assign gold medal to a given participant
     * @param p 
     */
    public static void assignGoldToParticipant(Participant p){
        p.setGold(p.getGold()+1);
    }
    

    
    
    
    // ***** ACCESS METHODS (GETS) *****
    public String getName() {
        return name;
    }
    
    public Delegation getDelegation() {
        return delegation;
    }
    
    public int getGold() {
        return gold;
    }
    
    public int getSilver() {
        return silver;
    }
    
    public int getBronze() {
        return bronze;
    }
    
    // ***** MODIFICATION METHODS (SETS) *****
    public void setName(String name) {
        this.name = name;
    }
    
    public void setDelegation(Delegation delegation) {
        this.delegation = delegation;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
    
    public void setSilver(int silver) {
        this.silver = silver;
    }
    
    public void setBronze(int bronze) {
        this.bronze = bronze;
    }
    
    // ***** ACCESSORY METHODS *****
    
    // toString
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nAthlete: ");
        s.append(name);
        s.append("\nGold medals: ");
        s.append(gold);
        s.append("\nSilver medals: ");
        s.append(silver);
        s.append("\nBronze medals: ");
        s.append(bronze);
        s.append(delegation);
        return s.toString();
    }
}
