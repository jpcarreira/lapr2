/* 
 * LAPR2 PROJECT
 * INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO
 * JUNE 2012
 * 
 * @authors: Diogo Leite, João Carreira, Paulo Ponciano
 * 
 */


/* TO DO LIST:
 *  1) add equals() 
 * 
 * 
 */

package Library;

import java.io.Serializable;

/**
 * CLASS SPORT: creates objects for each sport of the Olympic Games
 * 
 */

public class Sport  implements Serializable {

    //  *** CLASS VARIABLES ***

    
    //  *** INSTANCE VARIABLES ***

    // sport's name (e.g., Athletics)
    private String sport;

    //  *** CONSTRUCTORS ***

    
    /**
     * complete constructor
     * @param sport 
     */
    public Sport(String sport){
        setSport(sport);
    }

    //  *** CLASS METHODS ***

    
    /**
     * Method to assign medals for a given for a given sport
     * @param r
     * @param e
     * @param d
     * @param s 
     */
    public static void assignMedalsForSportForSport(Container<Result> r, Container<Event> e, Container<Delegation> d, String s){
        Container<Result> copy = filterBySport(r,s);
        Result.assignMedalsForSport(copy,e,d);
    }
    
    
    /**
     * Method to filter results for a given sport
     * @param r
     * @param s
     * @return 
     */
    public static Container<Result> filterBySport(Container<Result> r, String s){
        Container<Result> copy = new Container<Result>();
        for(int i=0; i<r.getsizeActual();i++){
            if(r.get(i).getEvent().getDiscipline().getSport().getSport().equalsIgnoreCase(s))
                copy.add(r.get(i));
        }
        return copy;
    }
    
    

    //  *** INSTANCE METHODS ***

    //      *** ACCESS METHODS (GETS) ***

    public String getSport() {
        return sport;
    }
    
        
    //      *** MODIFICATION METHODS (SETS) ***

    public void setSport(String sport) {
        this.sport = sport;
    }
    
        
    //      *** MÉTODOS COMPLEMENTARES E AUXILIARES ***

    // toString
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("\nSport: ");
        s.append(sport);
        return s.toString();
    }
    

}
