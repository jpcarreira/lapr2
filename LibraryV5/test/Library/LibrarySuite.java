/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Library;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author joaoana
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({Library.SportTest.class, Library.ContainerTest.class, Library.DisciplineTest.class, Library.CsvImportTest.class, Library.VerificacoesTest.class, Library.EventTest.class, Library.DelegationTest.class, Library.ResultTest.class, Library.ParticipantTest.class, Library.test2Test.class, Library.TestTest.class})
public class LibrarySuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
