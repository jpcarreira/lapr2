/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Library;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pauloponciano
 */
public class ContainerTest {
    
    /**
     * Test of setsize method, of class Container.
     */
    @Test
    public void testSetsize() {
        Container c = new Container();
        c.add("1");
        c.add("2");
        c.add("3");
        c.add("4");
        c.add("5");
        c.add("6");
        c.add("7");
        c.add("8");
        
        int expected = 10;
        int result = c.size();
         assertEquals(expected, result);
    }

    /**
     * Test of set method, of class Container.
     */
    @Test
    public void testSet() {
       Container c = new Container();
       c.add(1);
       c.add(2);
       c.add(3);    
       c.set(4, 1);
       
        int expected = 4;
        int result = (Integer) c.get(1);
        Assert.assertEquals(expected, result);
       
    }

    /**
     * Test of get method, of class Container.
     */
    @Test
    public void testGet() {
        Container c = new Container();
        c.add("1");
        c.add("2");
        c.add("3");
        Object expected = "2";
        Object result = c.getElemento(2);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of size method, of class Container.
     */
    @Test
    public void testSize() {
        Container c = new Container();
        c.add("1");
        c.add("2");
        c.add("3");
        c.add("4");
        c.add("5");
        c.add("6");
        c.add("7");
        c.add("8");
        
        int expected = 10;
        int result = c.size();
         assertEquals(expected, result);
        
    }


    /**
     * Test of getElemento method, of class Container.
     */
    @Test
    public void testGetElemento() {
        Container c = new Container();
        c.add("1");
        c.add("2");
        c.add("3");
        Object expected = "2";
        Object result = c.getElemento(2);
        Assert.assertEquals(expected, result);
        
        
    }

    /**
     * Test of reSize method, of class Container.
     */
    @Test
    public void testReSize() {
       Container c = new Container();
        c.setsize(10);
        c.setsize(100);
        int expected = 100;
        int result = c.size();
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of delete method, of class Container.
     */
    @Test
    public void testDelete() {
        Container c = new Container();
        c.add("1");
        c.add("2");
        c.add("3");
        c.add("4");
        c.add("5");
        c.add("6");
        c.add("7");
        c.add("8");
        c.add("9");
        c.add("10");
        c.delete(1);
        c.delete(2);
        Container c1 = new Container();
        c1.add("3");
        c1.add("4");
        c1.add("5");
        c1.add("6");
        c1.add("7");
        c1.add("8");
        c1.add("9");
        c1.add("10");

        String expected = c1.toString();

        String result = c.toString();
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of empty method, of class Container.
     */
    @Test
    public void testEmpty() {
        Container c = new Container();
         Assert.assertTrue(c.empty());
    }

    /**
     * Test of list method, of class Container.
     */
    @Test
    public void testList() {
         Container c = new Container();
        System.out.println("Este é o método Listar.");
        System.out.println("O contentor tem 4 linhas.");

        c.add("1ª linha");
        c.add("2ª linha");
        c.add("3ª linha");
        c.add("4ª linha");
        c.list();
       
    }

    /**
     * Test of toString method, of class Container.
     */
    @Test
    public void testToString() {
        Container c = new Container();
        c.add("1");
        c.add("2");
        c.add("3");
        String expected = "";
        for (int i = 0; i < c.getsizeActual(); i++) {
            expected += c.get(i);
            if (i != c.getsizeActual()-1) {
                expected += ", ";
            }
        }

        String result = c.toString();
        Assert.assertEquals(expected, result);
        
    }

    /**
     * Test of DeleteActualPos method, of class Container.
     */
    @Test
    public void testDeleteActualPos() {
        Container c = new Container();
        c.add("1");
        c.add("2");
        c.add("3");
        String expected = c.toString();

       
        c.DeleteActualPos();
         String result = c.toString();
        Assert.assertEquals(expected, result);
        
        
    }

    /**
     * Test of deleteAll method, of class Container.
     */
    @Test
    public void testDeleteAll() {
        System.out.println("deleteAll");
        Container c = new Container();
        c.add(1);
        c.deleteAll();
        boolean expected = true;
        boolean result = c.empty();
        
        Assert.assertEquals(expected, result);
        
    }

}
