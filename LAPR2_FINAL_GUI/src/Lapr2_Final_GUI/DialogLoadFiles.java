package Lapr2_Final_GUI;

import Library.CsvImport;
import java.awt.BorderLayout;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class DialogLoadFiles extends JDialog {
    
     public int verif;

    public DialogLoadFiles(JFrame pai, String titulo) {
        super(pai, titulo);

        // CENTER DO BORDERLAYOUT***********************************************
        JPanel pList = new JPanel(new GridLayout(8, 2));
        pList.setBorder(new EmptyBorder(15, 0, 10, 10));


        JPanel p1 = new JPanel();
        JButton btLoadDel = new JButton("Load Delegations");
        p1.add(btLoadDel);
        p1.setBorder(new EmptyBorder(10, 10, 10, 10));

        JPanel p2 = new JPanel();
        JButton btLoatParticipant = new JButton("Load athletes/teams");
        p2.add(btLoatParticipant);
        p2.setBorder(new EmptyBorder(10, 10, 10, 10));

        JPanel p3 = new JPanel();
        JButton btLoadSports = new JButton("Load Sports");
        p3.add(btLoadSports);
        p3.setBorder(new EmptyBorder(10, 10, 10, 10));

        JPanel p4 = new JPanel();
        JButton btLoadDiscipline = new JButton("Load Disciplines");
        p4.add(btLoadDiscipline);
        p4.setBorder(new EmptyBorder(10, 10, 10, 10));

        JPanel p5 = new JPanel();
        JButton btLoadEvent = new JButton("Load Events");
        p5.add(btLoadEvent);
        p5.setBorder(new EmptyBorder(10, 10, 10, 10));
        
        JPanel p6 = new JPanel();
        JButton btLoadResult = new JButton("Load Results");
        p6.add(btLoadResult);
        p6.setBorder(new EmptyBorder(10, 10, 10, 10));
        
        pList.add(p1);
        pList.add(p2);
        pList.add(p3);
        pList.add(p4);
        pList.add(p5);
        pList.add(p6);
//******- Delegation
        btLoadDel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                String path = ImportFiles.import_file(e);
                // Verifica nome ficheiro
                int i = verifi_deleg(path);
                try {
                    if(i!= -1)
                    {
                        //***** Metodos para guardar as cenas
                    CsvImport.save_deleg(path, Main.getDelegation());
                    JOptionPane.showMessageDialog(null,"Load successful!","Load Data",JOptionPane.PLAIN_MESSAGE);
                    }
                    else
                        JOptionPane.showMessageDialog(null, "File Not supported");
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Can't Find File");
                }
            }
        });
        //***********************individ
        btLoatParticipant.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String path = ImportFiles.import_file(e);
                //**** verifica nome ficheiro
                int i = verifi_result(path);
                
                try {
                    if(i!= -1)
                    {
                        //metodo para inserir
                    CsvImport.save_individ(path,Main.getParticipant(), Main.getDelegation());
                    JOptionPane.showMessageDialog(null,"Load successful!","Load Data",JOptionPane.PLAIN_MESSAGE);
                    //System.out.println(Main.getParticipant());
                    }
                    else
                        JOptionPane.showMessageDialog(null, "File Not supported");
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Can't Find File");
                }
                //System.out.println(Main.getParticipant());
            }
            });
        
        //************Sport
        btLoadSports.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String path = ImportFiles.import_file(e);
                //verifica nome do ficheiro
                int i = verifi_disci(path);
                
                try {
                    if(i!= -1)
                    {
                        //System.out.println("ola");
                    CsvImport.save_sport_cat(path, Main.getSport());
                    JOptionPane.showMessageDialog(null,"Load successful!","Load Data",JOptionPane.PLAIN_MESSAGE);
                    //System.out.println(Main.getSport());
                    }
                    else
                        JOptionPane.showMessageDialog(null, "File Not supported");
                } catch (FileNotFoundException ex) {
                    //JOptionPane.showMessageDialog(null, "Can't Find File");
                }
            }
        });

//****************Disciplin
        btLoadDiscipline.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String path = ImportFiles.import_file(e);
                // verifica nome ficheiro
                int i = verifi_disci(path);
                
                try {
                    if(i!= -1)
                    {
                        //guardar no array list
                    CsvImport.save_discipline_content(path, Main.getDiscipline(), Main.getSport());
                    JOptionPane.showMessageDialog(null,"Load successful!","Load Data",JOptionPane.PLAIN_MESSAGE);
                        //System.out.println(Main.getDiscipline());
                    }
                    else
                        JOptionPane.showMessageDialog(null, "File Not supported");
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Can't Find File");
                }
            }
        });
        
        //****************Event
        btLoadEvent.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String path = ImportFiles.import_file(e);
                int i = verifi_event(path);
                
                try {
                    if(i!= -1)
                    {
                    CsvImport.save_event(path, Main.getEvent(), Main.getDiscipline());
                    JOptionPane.showMessageDialog(null,"Load successful!","Load Data",JOptionPane.PLAIN_MESSAGE);
                    //System.out.println(Main.getEvent());
                    }
                    else
                        JOptionPane.showMessageDialog(null, "File Not supported");
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Can't Find File");
                }
            }
        });
        
        
        //****************Result
        
        //(String caminho, Container<Result> resultados, Container<Event> eventos, Container<Participant> competidor, Container<Discipline> disciplina)
        btLoadResult.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String path = ImportFiles.import_file(e);
                int i = verifi_result(path);
                //System.out.println(i + " asdsdsddadss");
                try {
                    if(i!= -1)
                    {
                        //System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
                    CsvImport.save_result(path, Main.getResult(), Main.getEvent(), Main.getParticipant(), Main.getDiscipline());
                    JOptionPane.showMessageDialog(null,"Load successful!","Load Data",JOptionPane.PLAIN_MESSAGE);
                    //System.out.println(Main.getResult());
                    }
                    else
                        JOptionPane.showMessageDialog(null, "File Not supported");
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null, "Can't Find File");
                }
            }
        });

        //**********************************************************************
        JPanel pOK = new JPanel();
        JButton bCANCEL = new JButton("Done");

        bCANCEL.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });


        pOK.add(bCANCEL);

        add(pList, BorderLayout.CENTER);
        add(pOK, BorderLayout.SOUTH);


        getRootPane().setDefaultButton(bCANCEL);
        // settings gerais da janela
        pack();
        setSize(400, 400);
        setResizable(true);
        setLocation(pai.getX() + 50, pai.getY() + 50);
        setVisible(true);
    }
    
    
    
     //Verificações ficheiros de entrada
    //Delegaçoes
    public int verifi_deleg(String caminho) {
        verif = caminho.indexOf("IOC_Codes");
        return verif;
    }

    //resultados e jogadores
    //resultados e jogadores
    public int verifi_result(String caminho) {
        verif = caminho.indexOf("Men");
        if (verif == -1) {
            verif = caminho.indexOf("Women");
        }
        return verif;
    }

    //disciplinas
   public int verifi_disci(String caminho) {
        verif = caminho.indexOf("IOC_Sports");
        return verif;
    }

    //disciplinas
   public int verifi_event(String caminho) {
        verif = caminho.indexOf("IOC_Sports_OG");
        return verif;
    }
}



