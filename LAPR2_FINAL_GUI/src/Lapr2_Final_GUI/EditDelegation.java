/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Lapr2_Final_GUI;

import Library.Delegation;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Diogo Leite
 */
public class EditDelegation extends JDialog{
    
    public EditDelegation(JFrame pai, String titulo) {
        super(pai, titulo, true);


        // CENTER DO BORDERLAYOUT***********************************************
        JPanel pList = new JPanel(new BorderLayout());
        Delegation.sortDelegationsAlpha(Main.delegation);
        final JComboBox delegation_list = new JComboBox(Main.DelegationToString(Main.getDelegation()));
        final TextField edit_deleg = new TextField(25);
        //pCombo.setBorder(new EmptyBorder(5,0,5,0));

        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JButton dele = new JButton("Edit");
        // SOUTH DO BORDERLAYOUT************************************************
        JPanel pOK = new JPanel();
        JButton btOK = new JButton("OK");
        JPanel edit = new JPanel(new GridLayout(1,2));
        JPanel pCombo = new JPanel(new GridLayout(1, 2));
        edit.add(delegation_list);
        edit.add(edit_deleg);
        pList.add(edit);
        pCombo.add(cancel);
        pCombo.add(dele);
        pList.add(pCombo, BorderLayout.SOUTH);
        add(pList, BorderLayout.SOUTH);
        add(edit, BorderLayout.NORTH);
        dele.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int resposta = JOptionPane.showConfirmDialog(null, "Are you sure you want to edit?", "Confir Editing", JOptionPane.YES_OPTION);
                if (resposta == JOptionPane.YES_NO_OPTION) {
                String a = edit_deleg.getText().toString();
                for (int i = 0; i < Main.delegation.getsizeActual(); i++) {
                    if(Main.delegation.get(i).getDelegation().toString().compareTo(delegation_list.getSelectedItem().toString())==0){
                        //System.out.println("ola");
                        //System.out.println(Main.delegation.get(i).getDelegation().toString() + " selest this");
                        Main.delegation.get(i).setDelegation(a);
                        //System.out.println(Main.delegation.get(i).getDelegation().toString() + " selest this");
                        break;
                       
                    }
                    
                }
                dispose();
            }
            }
        });
        // settings gerais da janela
//        pack();
        setSize(300, 300);
        setLocation(pai.getX() + 50, pai.getY() + 50);
        setVisible(true);
    }
}
