/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Lapr2_Final_GUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class DeletDiscipline extends JDialog {

    public DeletDiscipline(JFrame pai, String titulo) {
        super(pai, titulo, true);


        // CENTER DO BORDERLAYOUT***********************************************
        JPanel pList = new JPanel(new BorderLayout());
        final JComboBox delegation_list = new JComboBox(Main.DisciplineToString(Main.getDiscipline()));
        //pCombo.setBorder(new EmptyBorder(5,0,5,0));

        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JButton dele = new JButton("Delete");
        // SOUTH DO BORDERLAYOUT************************************************
        JPanel pOK = new JPanel();
        JButton btOK = new JButton("OK");
        JPanel pCombo = new JPanel(new GridLayout(1, 2));
        pList.add(delegation_list);
        pCombo.add(cancel);
        pCombo.add(dele);
        pList.add(pCombo, BorderLayout.SOUTH);
        add(pList, BorderLayout.CENTER);
        dele.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                
                int resposta = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete ?", "Confirmation", JOptionPane.YES_OPTION);
                if (resposta == JOptionPane.YES_NO_OPTION) {
                //System.out.println(Main.result);
                String ola = delegation_list.getSelectedItem().toString();
                //System.out.println(ola);
                int j = 0;
                for (int i = 0; i < Main.discipline.getsizeActual(); i++) {
                    if(Main.discipline.get(i).getDiscipline().compareTo(ola)==0){
                        //System.out.println(Main.discipline.get(i));
                        Main.discipline.delete(i);
                        //System.out.println("oi gato");
                    }
                }
                }
                Main.discipline.toString();
                

                //System.out.println(ola + " pais");
            }
        });
        // settings gerais da janela
//        pack();
        setSize(300, 300);
        setLocation(pai.getX() + 50, pai.getY() + 50);
        setVisible(true);
    }
    
}
