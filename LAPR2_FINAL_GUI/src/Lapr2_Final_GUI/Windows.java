package Lapr2_Final_GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Serializable;
import javax.swing.*;
//import lapr2gui.Classification_Nation;
//import lapr2gui.DialogListDelegations;


/*
 * MAIN WINDOW
 *
 */
public class Windows extends JFrame implements Serializable {

    //  *** CLASS VARIABLE***
    //  *** INSTANCE VARIABLES ***
    //  *** CONSTRUCTORS **
    // window constructor
    public Windows(String titulo) {

        //window title = superclass title
        super(titulo);
        //String verificações

        // declarate MenuBars and Menus
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;

        // create menubar
        menuBar = new JMenuBar();

        /*
        setJMenuBar(menuBar);
        menuBar.setBackground(Color.CYAN);
         */
        // MAIN MENU*******************************************************************************************************************************************************************************************************
        menu = new JMenu("Main Menu");
        menu.setMnemonic('M');
        menuBar.add(menu);

        // menuItens CARREGAR DELEGAÇÃO
        menuItem = new JMenuItem("Load ", 'L');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl L"));
        // actionListener para carregar delegações a partir de txt
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                DialogLoadFiles a = new DialogLoadFiles(Windows.this, "Load CSV files");


            }
        });
        menu.add(menuItem);


        // menuItens GRAVAR
        menuItem = new JMenuItem("Save", 'S');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Main.gravarEstado();
            }
        });
        menu.add(menuItem);

        // menuItens ELIMINAR TUDO
        menuItem = new JMenuItem("Delete", 'D');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl D"));
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                eliminar();
            }
        });
        menu.add(menuItem);

        // separador
        menu.addSeparator();

        // menuItens EXIT 
        menuItem = new JMenuItem("Exit", 'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fechar();
            }
        });
        menu.add(menuItem);
        //**********************************************************************************************************************************************************************************************************************************************

        // menu DATA MANAGEMENT*******************************************************************************************************************************************************************************************************************************
        menu = new JMenu("Data Management");
        menu.setMnemonic('D');
        menuBar.add(menu);

        // submenu DELEGATIONS
        JMenu submenu = new JMenu("Delegations");
        submenu.setMnemonic('D');

        // menuItens EDIT DELEGATION IN MENU DELEGATIONS
        menuItem = new JMenuItem("Edit Delegations", 'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
        // actionListener EDIT DELEGATION
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                EditDelegation a = new EditDelegation(Windows.this, "Edit Delegation");

            }
        });

        submenu.add(menuItem);

        // menuItens DELETE DELEGATION IN MENU DELEGATION
        menuItem = new JMenuItem("Delete Delegations", 'D');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));
        // actionListener DELETE DELEGATION
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DeletDelegation a = new DeletDelegation(Windows.this, "Delete Delegation");
            }
        });
        submenu.add(menuItem);
        menu.add(submenu);

        // menuItens LIST ALL THE DELEGATIONS
        menuItem = new JMenuItem("List Delegations", 'E');
        // actionListener LIST DELEGATIONS
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List_Delegation dialog = new List_Delegation(Windows.this, "List Delegation");
            }
        });
        submenu.add(menuItem);
        menu.add(submenu);
        //**********************************************************************************************************************************************************************************************************************


        // menu DISCIPLINE*************************************************************************************************************************************************************************************
        JMenu subMenuMod = new JMenu("Discipline");
        menu.setMnemonic('S');
        menu.add(subMenuMod);



        // menuItens EDIT DISCIPLINE
        menuItem = new JMenuItem("Edit Discipline", 'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl N"));
        // actionListener EDIT DISCIPLINE
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                EditDisciplin a = new EditDisciplin(Windows.this, "Edit Discipline");
            }
        });
        subMenuMod.add(menuItem);

        // menuItens DELETE DISCIPLINE
        menuItem = new JMenuItem("Delete Discipline", 'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Z"));
        // actionListener DELETE DISCIPLINE
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DeletDiscipline a = new DeletDiscipline(Windows.this, "Delete Discipline");
            }
        });
        subMenuMod.add(menuItem);


        // menuItem LISTS DISCIPLINE
        menuItem = new JMenuItem("List Discipline", 'L');
        // actionListener LIST DISCIPLINE
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List_Discipline a = new List_Discipline(Windows.this, "List Discipline");
            }
        });
        subMenuMod.add(menuItem);
        //**********************************************************************************************************************************************************************************************************************

        //**********************************************************************************************************************************************************************************************************************

        //MENU Sport
        JMenu subMenuProva = new JMenu("Sport");
        menu.setMnemonic('E');
        menu.add(subMenuProva);


        // menuItens EDIT SPORT
        menuItem = new JMenuItem("Edit Sport", 'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
        // actionListener EDIT SPORT
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                editSport a = new editSport(Windows.this, "Edit Sport");
            }
        });
        subMenuProva.add(menuItem);

        // menuItens DELETE SPORT
        menuItem = new JMenuItem("Delete Sport", 'Z');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Z"));
        // actionListener DELETE SPORT
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DeletSport a = new DeletSport(Windows.this, "Delete Sport");
            }
        });
        subMenuProva.add(menuItem);


        // menuItem LIST SPORT
        menuItem = new JMenuItem("List Sport", 'E');
        // actionListener LISTS SPORT
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List_Sport a = new List_Sport(Windows.this, "List Sport");
            }
        });
        subMenuProva.add(menuItem);
        menu.add(subMenuProva);
//*******************************MENU PARTICIPANT

        //MENU participant
        JMenu subMenuParticipant = new JMenu("Participant");
        menu.setMnemonic('E');
        menu.add(subMenuParticipant);


        // menuItens EDIT participant
        menuItem = new JMenuItem("Edit participant", 'E');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
        // actionListener EDIT Participant
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                editParticipant a = new editParticipant(Windows.this, "Edit participant");
            }
        });
        subMenuParticipant.add(menuItem);

        // menuItens DELETE Participant
        menuItem = new JMenuItem("Delete participant", 'Z');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Z"));
        // actionListener DELETE participant
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DeletParticipant a = new DeletParticipant(Windows.this, "Delete participant");
            }
        });
        subMenuParticipant.add(menuItem);


        // menuItem LIST participant
        menuItem = new JMenuItem("List participant", 'E');
        // actionListener LISTS EVENTS
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List_Participant a = new List_Participant(Windows.this, "List Participant");
            }
        });
        subMenuParticipant.add(menuItem);
        menu.add(subMenuParticipant);

        // menu RESULTS*******************************************************************************************************************************************************************************************************************************************************
        menu = new JMenu("Results");
        menu.setMnemonic('R');
        menuBar.add(menu);

        // submenu DISPLAY RESULTS
        JMenu submenu4 = new JMenu("Medal Rankings");
        submenu4.setMnemonic('R');

        // menuItens RESULTS NATIONS
        menuItem = new JMenuItem("Nation", 'N');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl N"));
        // actionListener RESULTS NATIONS
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Classification_Nation a = new Classification_Nation(Windows.this, "Nation Results");
            }
        });
        submenu4.add(menuItem);

//        // menuItens RESULTS ATHLETE
//        menuItem = new JMenuItem("Athlete", 'A');
//        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));
//        // actionListener RESULTS ATHLETE
//        menuItem.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                /*
//                 * if(Main.getResultado().isEmpty())
//                 * JOptionPane.showMessageDialog(Windows.this,"Não existem
//                 * Resultados!","Editar Resultado",JOptionPane.WARNING_MESSAGE);
//                 * else{ Object obj =
//                 * JOptionPane.showInputDialog(Windows.this,"Escolha o
//                 * Resultado:","Editar
//                 * Resultado",JOptionPane.PLAIN_MESSAGE,null,Main.nomesResultados(),Main.nomesResultados()[0]);
//                 * if(obj!=null){ Resultado r = Main.resultPorNome((String)obj);
//                 * try{ int res =
//                 * Integer.parseInt(JOptionPane.showInputDialog(Windows.this,"Indique
//                 * novo resultado final:","Editar
//                 * Resultado",JOptionPane.QUESTION_MESSAGE)); // atribui
//                 * medalhas assim que é editado um resultado
//                 * Delegacao.limparMedalhas(Main.getDelegacao());
//                 * r.setResult(res);
//                 * Resultado.atribuirMedalha(Main.getResultado()); }
//                 *
//                 * catch(NumberFormatException exc){
//                 * JOptionPane.showMessageDialog(Windows.this,"Resultado tem que
//                 * ser um valor númerico!","Criar
//                 * Resultado",JOptionPane.WARNING_MESSAGE); }
//                 * catch(ResultadoIncorrecto exc){
//                 * JOptionPane.showMessageDialog(Windows.this,exc.getMessage(),"Criar
//                 * Resultado",JOptionPane.WARNING_MESSAGE); } } }
//                 */
//            }
//        });
//        submenu4.add(menuItem);

        // menuItens RESULTS SPORTS
        menuItem = new JMenuItem("Sports", 'S');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        // actionListener RESULTS SPORTS
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Classifications_Sport a = new Classifications_Sport(Windows.this, "Sport Results");
            }
        });
        submenu4.add(menuItem);


        // menuItens RESULTS SPORTS
        menuItem = new JMenuItem("Discipline", 'S');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        // actionListener RESULTS SPORTS
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Classification_Discipline a = new Classification_Discipline(Windows.this, "Discipline Results");
            }
        });
        submenu4.add(menuItem);


        menu.add(submenu4);
        //separador
        //menu.addSeparator();

        // submenu DISPLAY RESULTS
        JMenu submenu5 = new JMenu("Display Results");
        submenu5.setMnemonic('R');

        // menuItem RESULTS TO SCREEN
        menuItem = new JMenuItem("Screen", 'S');
        // actionListener DISPLAY RESULTS TO SCREEN
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //DialogListarResultados dialog = new DialogListarResultados(Windows.this, "Listar Resultados");
            }
        });
        submenu5.add(menuItem);

        //meuItem RESULTS TO FILE
        menuItem = new JMenuItem("File", 'F');
        // actionListener DISPLAY RESULTS TO FILE
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //Main.listarResultFicheiro();
            }
        });

        submenu5.add(menuItem);
        menu.add(submenu4);
        //**********************************************************************************************************************************************************************************************************************************************



//
//        // submenu DISPLAY RESULTS
//        menuItem = new JMenuItem("Statistics");
//        menuItem.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                int i = 0;
//                boolean flag = false;
//
//                do {
//                    i = Integer.parseInt(JOptionPane.showInputDialog("Please select the number of delegations \n         Minimum 2 and Maximum 5"));
//                    if (i < 2 || i > 5) {
//                        JOptionPane.showMessageDialog(null, "You Need to Select At Least Two Delegations", "Error", JOptionPane.WARNING_MESSAGE);
//                        break;
//                    } else {
//                        flag = true;
//                    }
//                } while (i < 2 || i > 5);
//                if (flag) {
//                    DisplayStats a = new DisplayStats(Windows.this, "Statistics", i);
//                }
//
//            }
//        });
//        menu.add(menuItem);


        //**********************************************************************************************************************************************************************************************************************************************


        // menu ABOUT*******************************************************************************************************************************************************************************************************
        menu = new JMenu("About");
        menu.setMnemonic('A');
        menuBar.add(menu);

        // menuItens AUTHORS
        menuItem = new JMenuItem("Authors", 'a');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));
        menuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(Windows.this, "Software developed by:\nPaulo Ponciano (nº 1100456)\nJoão Carreira (nº 1111168)\nDiogo Leite (nº 1100570)\n\nLAPR2\nLicenciatura em Eng. Informática\nISEP - Porto, June 2012", "Authors", JOptionPane.PLAIN_MESSAGE);
            }
        });
        menu.add(menuItem);
        //**************************************************************************************************************************************************************************************


        // ADD menuBar
        setJMenuBar(menuBar);

        // CONFIGURE MAIN WINDOW
        add(new JLabel(new ImageIcon("logo.jpg")));
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });
        add(new JLabel(new ImageIcon("background2.png")));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();
        setSize(850, 500);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    //  *** CLASS METHODS ***
    // EXIT METHOD
    private void fechar() {
        Object opSimNao[] = {"Yes", "No"};
        if (JOptionPane.showOptionDialog(Windows.this, "Confirm quit?", "Olympics Management Software", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == 0) {//Main.gravarEstado();
            Main.gravarEstado();
            dispose();
        }
    }

    // SAVE METHOD
    private void gravar() {
        Object opSimNao[] = {"Yes", "No"};
        //if(JOptionPane.showOptionDialog(Windows.this,"Pretende gravar todos os dados? \nNOTA: Dados anteriores serão re-escritos!","Gravar",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,opSimNao,opSimNao[1])==0)
        Main.gravarEstado();
    }

    // DELETE METHOD
    private void eliminar() {
        Object opSimNao[] = {"Yes", "No"};
        if (JOptionPane.showOptionDialog(Windows.this, "Confirm deleting all data? \nNOTE: Previous data will not be recovered!", "Delete Data", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == 0) {
            /*
             * Main.getDelegacao().clear(); Main.getModalidade().clear();
             * Main.getResultado().clear(); Main.gravarEstado();
             */
        }
    }
    /*
    //Verificações ficheiros de entrada
    //Delegaçoes
    public int verifi_deleg(String caminho) {
    verif_a = caminho.indexOf("IOC_Codes");
    return verif_a;
    }
    
    //resultados e jogadores
    public int verifi_result(String caminho) {
    verif_b = caminho.indexOf("IOC_Codes");
    if (verif_b != -1) {
    verif_b = -1;
    }
    return verif_b;
    }
    
    //disciplinas
    public int verifi_disci(String caminho) {
    verif_c = caminho.indexOf("IOC_Sports");
    return verif_c;
    }
    
    //disciplinas
    public int verifi_event(String caminho) {
    verif_d = caminho.indexOf("IOC_Sports_OG");
    return verif_d;
    }
     * */
}
