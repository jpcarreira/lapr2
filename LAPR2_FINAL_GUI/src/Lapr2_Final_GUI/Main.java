package Lapr2_Final_GUI;

import Library.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JOptionPane;


public class Main {

    private static Windows window = new Windows("Olympics Management Software");
    public static Container<Delegation> delegation = new Container<Delegation>();
    public static Container<Participant> participant = new Container<Participant>();
    public static Container<Sport> sport = new Container<Sport>();
    public static Container<Result> result = new Container<Result>();
    public static Container<Discipline> discipline = new Container<Discipline>();
    public static Container<Event> event = new Container<Event>();

    public static void main(String[] args) throws FileNotFoundException {
        lerEstadoAnterior();
    }

    public static Container<Delegation> getDelegation() {
        return delegation;
    }

    public static Container<Result> getResult() {
        return result;
    }

    public static Container<Sport> getSport() {
        return sport;
    }

    public static Container<Participant> getParticipant() {
        return participant;
    }

    public static Container<Discipline> getDiscipline() {
        return discipline;
    }

    public static Container<Event> getEvent() {
        return event;
    }

    // método que retorna array de strings com nomes de todas as delegações (usado para listar delegações)
    public static String[] DelegationToString(Container<Delegation> a) {

        String[] b = new String[a.getsizeActual()];
        for (int i = 0; i < b.length; i++) {
            b[i] = a.get(i).getDelegation();
            //String.format(" %s : %s ", nomeM);
        }
        return b;
    }

    // método que retorna array de strings com nomes de todas as modalidades (usado para listar modalidades)
    public static String[] SportsToString(Container<Sport> a) {

        String[] b = new String[a.getsizeActual()];
        for (int i = 0; i < b.length; i++) {
            b[i] = a.get(i).getSport();
            //***********************
            //String nomeM = a.get(i).getSport();
            //b[i] = String.format(" %s : %s ", nomeM);
        }
        return b;
    }
    
    // método que retorna array de strings com nomes de todas as Disciplinas (usado para listar delegações)
    public static String[] DisciplineToString(Container<Discipline> a) {

        String[] b = new String[a.getsizeActual()];
        for (int i = 0; i < b.length; i++) {
            b[i] = a.get(i).getDiscipline();
            //String.format(" %s : %s ", nomeM);
        }
        return b;
    }
    
    // método que retorna array de strings com nomes de todas as Disciplinas (usado para listar delegações)
    public static String[] ParticipantToString(Container<Participant> a) {

        String[] b = new String[a.getsizeActual()];
        for (int i = 0; i < b.length; i++) {
            b[i] = a.get(i).getName();
            //String.format(" %s : %s ", nomeM);
        }
        return b;
    }
    
    // método que retorna array de anos
    public static String[] AnosToString(Container<Event> a) {

        String[] b = new String[1];
        b[0]=String.valueOf(a.get(0).getYear());
        for (int i = 0; i < a.getsizeActual()-1; i++) {
                if(a.get(i).getYear()!=a.get(i+1).getYear()){
                    String[] c = new String[b.length+1];
                    System.arraycopy(b, 0, c, 0, b.length);
                    b = new String[c.length];
                    System.arraycopy(c, 0, b, 0, c.length);
                    b[b.length-1]=String.valueOf(a.get(i+1).getYear());
            }
            
            //String.format(" %s : %s ", nomeM);
        }
        return b;
    }

    // método que retorna array de strings com nomes de todas as modalidades (usado para listar resultados)
    public static String[] resultsToString(Container<Result> a) {
        String[] b = new String[a.getsizeActual()];
        for (int i = 0; i < a.size(); i++) {
            String nomeD = a.get(i).getParticipant().getDelegation().getDelegation();
            String nomeM = a.get(i).getEvent().getDiscipline().getSport().getSport();
            String nomeP = a.get(i).getEvent().getEvent();
            String x = a.get(i).getResult();
            b[i] = String.format(" %s : %s - %s : %dº lugar", nomeD, nomeM, nomeP, x);
        }
        return b;
    }

    /*
    public static Result resultPorNome(String s){
    String[] nomes = nomesResultados();
    int i=0;
    while(!nomes[i].equalsIgnoreCase(s))
    i++;
    return resultado.get(i);
    }
    
    public static String[] classifPorString(){
    String[] a = new String[delegacao.size()];
    for(int i=0; i<a.length; i++){
    String nome = delegacao.get(i).getPais();
    int medalha = Medalhas.totalMedalhas(delegacao.get(i));
    a[i]=String.format("%s >>>> %d medalhas",nome,medalha);
    }
    return a;
    }
    /*
    public static String[] classifPorString2(){
    String[] a = new String[delegacao.size()];
    for(int i=0; i<a.length; i++){
    String nome = delegacao.get(i).getPais();
    int[] medalha = Medalhas.arrayMedalhas(delegacao.get(i));
    a[i]=String.format("%s,%d,%d,%d",nome,medalha[0],medalha[1],medalha[2]);
    }
    return a;
    }
    public static String[] classifQualifPorString(){
    String[] a = new String[delegacao.size()];
    for(int i=0; i<a.length; i++){
    String nome = delegacao.get(i).getPais();
    int peso = Medalhas.totalMedalhasQual(delegacao.get(i));
    a[i]=String.format("%s >>>> %d pts",nome,peso);
    }
    return a;
    }
    
    // método que retorna array de strings com nomes de todas as delegações sem medalhas (usado para listar delegações)
    public static String[] nomesDelegacoesSemMedalhas(){
    String[] a = new String[delegacao.size()];
    for(int i=0; i<a.length; i++){
    if(Medalhas.totalMedalhas(delegacao.get(i))==0){
    String nome = delegacao.get(i).getPais();
    a[i]=String.format(nome);
    }
    }
    return a;
    }
    
     */
    public static void gravarEstado() {
        try {
            ObjectOutputStream out1 = new ObjectOutputStream(new FileOutputStream("delegation.bin"));
            out1.writeObject(delegation);
            out1.close();
            ObjectOutputStream out2 = new ObjectOutputStream(new FileOutputStream("sports.bin"));
            out2.writeObject(sport);
            out2.close();
            ObjectOutputStream out3 = new ObjectOutputStream(new FileOutputStream("events.bin"));
            out3.writeObject(event);
            out3.close();
            ObjectOutputStream out4 = new ObjectOutputStream(new FileOutputStream("results.bin"));
            out4.writeObject(result);
            out4.close();
            ObjectOutputStream out5 = new ObjectOutputStream(new FileOutputStream("participant.bin"));
            out5.writeObject(participant);
            out5.close();
        } 
        catch (IOException exc) {
            JOptionPane.showMessageDialog(window, "Status not loaded!",
                    "Close Application", JOptionPane.ERROR_MESSAGE);
        }
    }

    private static void lerEstadoAnterior() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("delegation.bin"));
            delegation = (Container<Delegation>) in.readObject();
            //in.close();
            ObjectInputStream in2 = new ObjectInputStream(new FileInputStream("sports.bin"));
            sport = (Container<Sport>) in2.readObject();
            //in2.close();
            ObjectInputStream in3 = new ObjectInputStream(new FileInputStream("events.bin"));
            event = (Container<Event>) in3.readObject();
            //in3.close();
            ObjectInputStream in4 = new ObjectInputStream(new FileInputStream("results.bin"));
            result = (Container<Result>) in4.readObject();
            //in4.close();
            ObjectInputStream in5 = new ObjectInputStream(new FileInputStream("participant.bin"));
            participant = (Container<Participant>) in5.readObject();
            //in5.close();
        } catch (IOException exc) {
            JOptionPane.showMessageDialog(window,
                    "Previous status not loaded.",
                    "Data loaded successufully",
                    JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException exc) {
            JOptionPane.showMessageDialog(window,
                    "Previous status not loaded.",
                    "Data loaded successufully",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public static String[] honorTable(Container<Delegation> d){
        String[] s = new String[d.getsizeActual()];
        for (int i=0; i<d.getsizeActual(); i++){
            String delegation = d.get(i).getDelegation();
            int gold = d.get(i).getGold();
            int silver = d.get(i).getSilver();
            int bronze = d.get(i).getBronze();
            s[i]=String.format("%s    %d gold  %d silver  %d bronze", delegation,gold,silver,bronze);
        }
        return s;
    }

}
