package Lapr2_Final_GUI;

import Library.Delegation;
import javax.swing.*;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;


public class DisplayStats extends JDialog {

    private JComboBox[] comboDeleg;
    private JComboBox comboAux = new JComboBox();
    private JLabel[] esp;
    private String[] aux = new String[5];
    private int cont = 0;

    // DISPLAY STATS CONSTRUCTER
    public DisplayStats(JFrame pai, String titulo, int number) {
        super(pai, titulo, true);

        esp = new JLabel[number];
        comboDeleg = new JComboBox[number];
        JPanel pBorder = new JPanel(new BorderLayout());
        JPanel pCombo = new JPanel(new GridLayout(11, 3));
        pCombo.setBorder(new EmptyBorder(5, 0, 5, 0));
        JLabel lb1 = new JLabel("Choose between 2 and 5 delegations: ");
        lb1.setHorizontalAlignment(SwingConstants.CENTER);
        pCombo.add(lb1);

        //COMBO BOX INTIALIZEo
        for (int i = 0; i < comboDeleg.length; i++) {
            Delegation.sortDelegationsAlpha(Main.getDelegation());
            comboDeleg[i] = new JComboBox(Main.DelegationToString(Main.getDelegation()));
            

        }
        for (int i = 0; i < comboDeleg.length; i++) {
            comboDeleg[i].setSelectedIndex(-1);
            comboDeleg[i].setMaximumRowCount(5);

        }


        //LABEL INITIALIZE AND ADD TO PANEL
        for (int i = 0; i < comboDeleg.length; i++) {
            pCombo.add(comboDeleg[i]);
            esp[i] = new JLabel("");
            pCombo.add(esp[i]);
        }


        //ADD BTN_OK TO PANEL
        JPanel pOk = new JPanel();
        pOk.setBorder(new EmptyBorder(10, 0, 10, 0));
        JButton btOk = new JButton("OK");
        btOk.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try
                {
                for (int i = 0; i < comboDeleg.length; i++) {
                    if(comboDeleg[i].getSelectedItem().toString().isEmpty())
                    {
                        //System.out.println(comboDeleg[i]);
                        JOptionPane.showMessageDialog(null, "You Need to Select At Least Two Delegations", "Error", JOptionPane.WARNING_MESSAGE);
                        dispose();
                    }
                    else
                    {
                        aux[i]= comboDeleg[i].getSelectedItem().toString();
                    }   
                }
                dispose();
                DialogModalidade a = new DialogModalidade(DisplayStats.this, "Statistics", aux, comboDeleg.length);
                }
                catch(NullPointerException ex)
                {
                    JOptionPane.showMessageDialog(null, "You need to select the correct number of delegations", "Error", JOptionPane.WARNING_MESSAGE);
                }
                

            }
        });
                
                
                
        JButton btCancel = new JButton("Cancel");
        btCancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });


        
       
        pOk.add(btOk);
         pOk.add(btCancel);

        getRootPane().setDefaultButton(btOk);
        pBorder.add(pCombo, BorderLayout.CENTER);
        //add(pCombo, BorderLayout.CENTER);

        add(pOk, BorderLayout.SOUTH);

        add(pBorder, BorderLayout.CENTER);

        setMinimumSize(
                new Dimension(500, 300));
        setLocation(pai.getX() + 50, pai.getY() + 50);
        setVisible(
                true);
    }
}
