/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Lapr2_Final_GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Diogo Leite
 */
public class List_Discipline extends JDialog {


    public List_Discipline(JFrame pai, String titulo) {
        super(pai, titulo, true);
        
        JList lista1 = new JList();
        JScrollPane lista1SP = new JScrollPane(lista1);
        lista1.setVisibleRowCount(7);
        lista1.setFixedCellHeight(20);
        lista1.setFixedCellWidth(120);
        lista1.setListData(Main.DisciplineToString(Main.discipline));

        
        //Janela
        JPanel pList = new JPanel(new BorderLayout());
        JLabel titulo_a = new JLabel("Discipline");
        JButton fechar = new JButton("Cancel");
        
        fechar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        pList.add(titulo_a, BorderLayout.NORTH);
        pList.add(lista1SP,BorderLayout.CENTER);
        pList.add(fechar, BorderLayout.SOUTH);
        add(pList);
        setSize(250, 250);
        setLocation(pai.getX() + 50, pai.getY() + 50);
        setVisible(true);


    }
    
    
    // método que retorna array de anos

}


