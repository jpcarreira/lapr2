/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Lapr2_Final_GUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class DeletSport extends JDialog {

    public DeletSport(JFrame pai, String titulo) {
        super(pai, titulo, true);


        // CENTER DO BORDERLAYOUT***********************************************
        JPanel pList = new JPanel(new BorderLayout());
        final JComboBox delegation_list = new JComboBox(Main.SportsToString(Main.getSport()));
        //pCombo.setBorder(new EmptyBorder(5,0,5,0));

        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        JButton dele = new JButton("Delete");
        // SOUTH DO BORDERLAYOUT************************************************
        JPanel pOK = new JPanel();
        JButton btOK = new JButton("OK");
        JPanel pCombo = new JPanel(new GridLayout(1, 2));
        pList.add(delegation_list);
        pCombo.add(cancel);
        pCombo.add(dele);
        pList.add(pCombo, BorderLayout.SOUTH);
        add(pList, BorderLayout.CENTER);
        dele.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println(Main.result);
                int resposta = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete?", "Confirm delete", JOptionPane.YES_OPTION);
                if (resposta == JOptionPane.YES_NO_OPTION) {
                String ola = delegation_list.getSelectedItem().toString();
                //System.out.println(ola);
                int j = 0;
                for (int i = 0; i < Main.sport.getsizeActual(); i++) {
                    if(Main.sport.get(i).getSport().compareTo(ola)==0){
                        //System.out.println(Main.sport.get(i));
                        Main.sport.delete(i);
                        //System.out.println("oi gato");
                    }
                }
                }
                Main.sport.toString();
                

                //System.out.println(ola + " pais");
            }
        });
        // settings gerais da janela
//        pack();
        setSize(300, 300);
        setLocation(pai.getX() + 50, pai.getY() + 50);
        setVisible(true);
    }
    
}

