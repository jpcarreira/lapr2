/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Lapr2_Final_GUI;

import Library.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

/**
 *
 * @author Diogo Leite
 */
public class export_HTML {

    public static void export_HTML(int[]  pos, String titulo, String nome_ficheiro, int a) throws FileNotFoundException {
        Date actuelle = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String dat = dateFormat.format(actuelle);

        Formatter fOut;

        fOut = new Formatter(new File(nome_ficheiro+".html"));
        fOut.format("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
        fOut.format("%n");
        fOut.format("<html>");
        fOut.format("%n");
        fOut.format("%n");
        fOut.format("<head>");
        fOut.format("%n");
        fOut.format("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" >");
        fOut.format("%n");
        fOut.format("<title> ");
        fOut.format("Exemplo_HTML");
        fOut.format(" </title>");
        fOut.format("%n");
        fOut.format("%n");
        fOut.format("</head>");
        fOut.format("%n");
        fOut.format("%n");
        fOut.format("<body>");
        fOut.format("%n");
        fOut.format("%n");
        fOut.format("<p align=\"center\">" + "JO " + "</p>");
        fOut.format("%n");
        fOut.format("%n");
        fOut.format(guardavector(pos, titulo, a));
        fOut.format("%n");
        fOut.format("<p align=right>"+ dat +"<p>");
        fOut.format("%n");
        fOut.format("</body>");
        fOut.format("%n");
        fOut.format("%n");
        fOut.format("</html>");

        fOut.close();
    }

    public static String guardavector(int[] posicao, String titulo, int linha) {
        String[][] linguas = new String[3][3];
        linguas[0][0]="Classification";
        linguas[0][1]="Designation";
        linguas[0][2]="Résultat";
        
        linguas[1][0]="Classificação";
        linguas[1][1]="Designação";
        linguas[1][2]="Resultados";
        
        linguas[2][0]="Classification";
        linguas[2][1]="Designation";
        linguas[2][2]="Result";
        String msg = new String();
        msg += ("<table Align=\"center\" Border=\"3\">");
        msg += ("<thead>");
        msg += ("<tr>");
        msg += ("<FONT \" SIZE=5>");
        msg += ("<th width=220 align=\"center\">" + linguas[linha][0] + "</th>");
        msg += ("</FONT>");
        msg += ("<FONT \" SIZE=5>");
        msg += ("<th width=220 align=\"center\">" + linguas[linha][1] + "</th>");
        msg += ("</FONT>");
        msg += ("<FONT \" SIZE=5>");
        msg += ("<th width=220 align=\"center\">" + linguas[linha][2] + "</th>");
        msg += ("</FONT>");
        msg += ("</tr>\n");
        msg += ("</thead>\n");
        for (int i = 0; i < posicao.length; i++) {
            //String string = resultados[i];
            //primeira linha a amarelo
            if (posicao[i] == 1) {
                msg += ("<tr>");

                msg += ("<td height=10 bgcolor=\"yellow\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf(posicao[i]);
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("<td bgcolor=\"yellow\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf(Main.delegation.get(i).getDelegation());
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("<td bgcolor=\"yellow\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf("Gold: "+Main.delegation.get(i).getGold() +" Silver: "+Main.delegation.get(i).getSilver()+ " Bronze: "+ Main.delegation.get(i).getBronze());
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("</tr>\n");
                } 
            else if(posicao[i] == 2){
                msg += ("<tr>");

                msg += ("<td height=10 bgcolor=\"Silver\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf(posicao[i]);
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("<td bgcolor=\"Silver\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf(Main.delegation.get(i).getDelegation());
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("<td bgcolor=\"Silver\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf("Gold: "+Main.delegation.get(i).getGold() +" Silver: "+Main.delegation.get(i).getSilver()+ " Bronze: "+ Main.delegation.get(i).getBronze());
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("</tr>\n");
            }
            else if(i==2){
                msg += ("<tr>");

                msg += ("<td height=10 bgcolor=\"saddlebrown\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf(posicao[i]);
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("<td bgcolor=\"saddlebrown\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf(Main.delegation.get(i).getDelegation());
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("<td bgcolor=\"saddlebrown\">");
                msg += ("<FONT \" SIZE=4>");
                msg += ("<b>");
                msg += String.valueOf("Gold: "+Main.delegation.get(i).getGold() +" Silver: "+Main.delegation.get(i).getSilver()+ " Bronze: "+ Main.delegation.get(i).getBronze());
                msg += ("</b>");
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("</tr>\n");
            }
            
            else {
                msg += ("<tr>");

                msg += ("<td height=10>");
                msg += ("<FONT \" SIZE=3>");
                msg += String.valueOf(posicao[i]);
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("<td>");
                msg += ("<FONT \" SIZE=3>");
                msg += String.valueOf(Main.delegation.get(i).getDelegation());
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("<td>");
                msg += ("<FONT \" SIZE=3>");
                msg += String.valueOf("Gold: "+Main.delegation.get(i).getGold() +" Silver: "+Main.delegation.get(i).getSilver()+ " Bronze: "+ Main.delegation.get(i).getBronze());
                msg += ("</FONT>");
                msg += ("</td>\n");

                msg += ("</tr>\n");
            }
            

        }
        msg += ("</table>");
//        System.out.println(msg);
        return msg;
    }
}
