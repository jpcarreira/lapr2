package Lapr2_Final_GUI;

import Library.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Diogo Leite
 */
public class Classification_Discipline extends JDialog {

    private String test = "";

    public Classification_Discipline(JFrame pai, String titulo) {
        super(pai, titulo, true);
        final String desporto = "";
        final int[] posicoes = new int[Main.delegation.getsizeActual()];


        //janela
        //janela toda
        JPanel principal = new JPanel(new BorderLayout());
        final JComboBox lingua = new JComboBox();
        lingua.addItem("Français");
        lingua.addItem("Português");
        lingua.addItem("English");
        JLabel sport_lb = new JLabel("Sport");
        final JComboBox anos = new JComboBox(Main.AnosToString(Main.event));
        final JComboBox desportos = new JComboBox(Main.DisciplineToString(Main.discipline));
        JButton validar = new JButton("Ok");
        JPanel topo = new JPanel(new GridLayout(1, 4));
        topo.add(sport_lb);
        topo.add(anos);
        topo.add(desportos);
        topo.add(validar);
        final JLabel primeiro = new JLabel("1");

        final JLabel primeiro_nome = new JLabel();

        final JLabel segundo = new JLabel("2");
        final JLabel segundo_nome = new JLabel();

        final JLabel terceiro = new JLabel("3");
        final JLabel terceiro_nome = new JLabel();

        final JLabel quarto = new JLabel("4");
        final JLabel quarto_nome = new JLabel();

        final JLabel quinto = new JLabel("5");
        final JLabel quinto_nome = new JLabel();

        final JLabel sexto = new JLabel("6");
        final JLabel sexto_nome = new JLabel();

        final JLabel setimo = new JLabel("6");
        final JLabel setimo_nome = new JLabel("");

        final JLabel oitavo = new JLabel("7");
        final JLabel oitavo_nome = new JLabel("");

        final JLabel nono = new JLabel("8");
        final JLabel nono_nome = new JLabel("");

        final JLabel deximo = new JLabel("9");
        final JLabel deximo_nome = new JLabel("");



        validar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int ano = Integer.parseInt(anos.getSelectedItem().toString());
                String desporto = desportos.getSelectedItem().toString();
                //System.out.println("ano: " + ano + " Desporto: " + desporto);

                Delegation.clearMedals(Main.getDelegation());
                Container<Result> x = Delegation.filterByYear(Main.result, ano);//.filterByYear(Main.result, ano);
                Container<Result> y = Discipline.filterByDiscipline(x, desporto);
                Result.assignMedalsForSport(y, Main.getEvent(), Main.getDelegation());
                Delegation.rankDelegations(Main.delegation);
                //System.out.println(Main.delegation);
                //primeiro.setText("ano: " + ano + " Desporto: " + desporto);
                // ver impates

                int a = 1, b = 2;
                posicoes[0] = a;
                a=2;
                for (int i = 1; i < Main.delegation.getsizeActual(); i++) {
                    
                    if (Main.delegation.get(i).getGold() == Main.delegation.get(i - 1).getGold() && Main.delegation.get(i).getSilver() == Main.delegation.get(i - 1).getSilver() && Main.delegation.get(i).getBronze() == Main.delegation.get(i - 1).getBronze()) {
                        //System.out.println("igual : " + a);
                    
                    } else {
                        
                        a = b;
                        
                        //System.out.println("Valor seguint: "+a);
                    }
                    b++;
                    posicoes[i] = a;
                    
                }

                int q =posicoes.length;
                posicoes[q-1] = posicoes[q-2];
                
                String[] s1 = Main.honorTable(Main.delegation);
                primeiro_nome.setText(s1[0]);

                segundo.setText(String.valueOf(posicoes[1]));
                segundo_nome.setText(s1[1]);

                terceiro.setText(String.valueOf(posicoes[2]));
                terceiro_nome.setText(s1[2]);

                quarto.setText(String.valueOf(posicoes[3]));
                quarto_nome.setText(s1[3]);

                quinto.setText(String.valueOf(posicoes[4]));
                quinto_nome.setText(s1[4]);

                sexto.setText(String.valueOf(posicoes[5]));
                sexto_nome.setText(s1[5]);

                setimo.setText(String.valueOf(posicoes[6]));
                setimo_nome.setText(s1[6]);

                oitavo.setText(String.valueOf(posicoes[7]));
                oitavo_nome.setText(s1[7]);

                nono.setText(String.valueOf(posicoes[8]));
                nono_nome.setText(s1[8]);

                deximo.setText(String.valueOf(posicoes[9]));
                deximo_nome.setText(s1[9]);

            }
        });
        //meio


        JPanel meio = new JPanel(new GridLayout(10, 2));
        meio.add(primeiro);
        meio.add(primeiro_nome);
        meio.add(segundo);
        meio.add(segundo_nome);
        meio.add(terceiro);
        meio.add(terceiro_nome);
        meio.add(quarto);
        meio.add(quarto_nome);
        meio.add(quinto);
        meio.add(quinto_nome);
        meio.add(sexto);
        meio.add(sexto_nome);
        meio.add(setimo);
        meio.add(setimo_nome);
        meio.add(oitavo);
        meio.add(oitavo_nome);
        meio.add(nono);
        meio.add(nono_nome);
        meio.add(deximo);
        meio.add(deximo_nome);
        //
        JButton exportHTML = new JButton("Export HTML");
        exportHTML.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int a = lingua.getSelectedIndex();
                    export_HTML.export_HTML(posicoes, "ola", "Discipline", a);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Classification_Nation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        //topo.add(anos);
        principal.add(topo, BorderLayout.NORTH);
        principal.add(meio);
        //Ultimo botão
        JPanel ultima_linha = new JPanel(new GridLayout(1, 2));
        ultima_linha.add(exportHTML);
        ultima_linha.add(lingua);
        principal.add(ultima_linha, BorderLayout.SOUTH);
        add(principal, BorderLayout.CENTER);
        setSize(500, 500);
        setLocation(pai.getX() + 50, pai.getY() + 50);
        setVisible(true);

    }
    
}
