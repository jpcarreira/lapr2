/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Lapr2_Final_GUI;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import javax.swing.JFileChooser;

/**
 *
 * @author Diogo Leite
 */
public class ImportFiles implements Serializable  {

    public static String import_file(ActionEvent e) {
        int val = 0;
        String www = "";
        JFileChooser fi = new JFileChooser();
        fi.setDialogTitle("Recuperar Ficheiro");
        fi.setFileSelectionMode(JFileChooser.FILES_ONLY);
        val = fi.showSaveDialog(null);
        if (val == JFileChooser.APPROVE_OPTION) {
            fi.getSelectedFile().getAbsoluteFile();
            www = fi.getSelectedFile().getAbsoluteFile().toString();
            
        }
        return www;
    }
}
